<?php
namespace Insivia\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * Get gravatar image
 * 
 * @author dolly.aswin@gmail.com
 */
class Gravatar extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    
    /**
     * Get gravatar image
     * 
     * @param string $email
     * @param int    $size
     * @return string
     */
    public function __invoke($email, $size = null)
    {
        $email = strtolower(trim($email));
        $hash  = md5($email);
        $sizeParam = '?s=200';
        if ($size !== null) {
            $sizeParam = '?s=' . strval($size);
        }
        
        return "<img src=\"http://www.gravatar.com/avatar/" . $hash . $sizeParam . "\" />";
    }
}
