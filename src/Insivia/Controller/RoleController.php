<?php

namespace Insivia\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class RoleController extends AbstractActionController
{
    public function switcherAction()
    {
        $zfcUserAuthService = $this->getServiceLocator()->get('zfcuser_auth_service');
        if ($this->getRequest()->isPost() && $zfcUserAuthService->getIdentity() !== null) {
            $role = $this->getRequest()->getPost('role');
            $roleService = $this->getServiceLocator()->get('lisyn.role');
            $user = $zfcUserAuthService->getIdentity();
            // set logout from current role
            $roleService->setLogoutAuthLog($user);
            // login to new role
            $roleService->createAuthLog($user, $role);
            // redirect
            return $this->redirect()->toRoute($role);
        }
        
        return $this->redirect()->toRoute('home');
    }
}
