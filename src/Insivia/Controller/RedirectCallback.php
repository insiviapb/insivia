<?php

namespace Insivia\Controller;

use Zend\Mvc\Application;
use Zend\Mvc\Router\RouteInterface;
use Zend\Http\PhpEnvironment\Response;
use ZfcUser\Options\ModuleOptions;

/**
 * Buils a redirect response based on the current routing and parameters
 */
class RedirectCallback
{

    /** @var RouteInterface  */
    private $router;

    /** @var Application */
    private $application;

    /** @var ModuleOptions */
    private $options;

    /**
     * @param Application $application
     * @param RouteInterface $router
     * @param ModuleOptions $options
     */
    public function __construct(Application $application, RouteInterface $router, ModuleOptions $options)
    {
        $this->router = $router;
        $this->application = $application;
        $this->options = $options;
    }

    /**
     * @return Response
     */
    public function __invoke()
    {
        $routeMatch = $this->application->getMvcEvent()->getRouteMatch();
        $redirect = $this->getRedirect($routeMatch->getMatchedRouteName(), $this->getRedirectRouteFromRequest());

        $response = $this->application->getResponse();
        $response->getHeaders()->addHeaderLine('Location', $redirect);
        $response->setStatusCode(302);
        return $response;
    }

    /**
     * Return the redirect from param.
     * First checks GET then POST
     * @return string
     */
    private function getRedirectRouteFromRequest()
    {
        $request  = $this->application->getRequest();
        $redirect = $request->getQuery('redirect');
        if ($redirect && $this->useLocalUrl($redirect)) {
            return $redirect;
        }

        $redirect = $request->getPost('redirect');
        if ($redirect && $this->useLocalUrl($redirect)) {
            return $redirect;
        }

        return false;
    }

    /**
     * Check if the URL use local url or not
     * 
     * @param  $url
     * @return bool
     */
    private function useLocalUrl($url)
    {
        $localUrlPattern = '/^\/[a-z0-9\/\-]+/';
        if (!preg_match($localUrlPattern, $url)) {
            return false;
        }
        
        return true;
    }

    /**
     * Returns the url to redirect to based on current route.
     * If $redirect is set and the option to use redirect is set to true, it will return the $redirect url.
     *
     * @param string $currentRoute
     * @param bool $redirect
     * @return mixed
     */
    protected function getRedirect($currentRoute, $redirect = false)
    {
        $useRedirect = $this->options->getUseRedirectParameterIfPresent();
        $useLocalUrl = $this->useLocalUrl($redirect);
        if (!$useRedirect || !$useLocalUrl) {
            $redirect = false;
        }

        if ($redirect !== false) {
            return $redirect;
        }

        switch ($currentRoute) {
            case 'zfcuser/register':
            case 'zfcuser/login':
            case 'signin':
                $route = 'consumer';
                if (SITE_ENV != 'consumer') {
                    $route = 'publisher';
                }
                return $this->router->assemble(array(), array('name' => $route));
                break;
            case 'zfcuser/logout':
                $route = ($redirect) ?: $this->options->getLogoutRedirectRoute();
                return $this->router->assemble(array(), array('name' => $route));
                break;
            case 'signout':
                
                $route = ($redirect) ?: 'signin';
                //remove persistent cookie upon logout
                $uri    = $this->application->getRequest()->getUri();
                $host   = $uri->getHost();
                setcookie('user', '', strtotime('-1 Year', time()), '/', $host);
                return $this->router->assemble(array(), array('name' => $route));
                break;
            default:
                if(strpos($currentRoute, 'register') !== FALSE) {
                    $route = ($redirect) ?: 'activate';
                    return $this->router->assemble(array(), array('name' => $route));
                }
                return $this->router->assemble(array(), array('name' => 'zfcuser'));
        }
    }
}
