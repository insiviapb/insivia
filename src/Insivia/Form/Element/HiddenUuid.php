<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Hidden as FormElementHidden;

/**
 * Class for Text Element lastName
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class HiddenUuid extends FormElementHidden
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('uuid');
    }
}
