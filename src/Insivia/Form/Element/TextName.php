<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Text;

/**
 * Class for Text Element TextName
 *
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
class TextName extends Text
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct($name);
        $this->setName($name)
            ->setOption('label', 'Text Name');
        
        foreach($options as $key => $value) {
            $this->setAttribute($key, $value);
        }
        
    }
}
