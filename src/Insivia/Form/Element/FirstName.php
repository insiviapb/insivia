<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Text;

/**
 * Class for Text Element firstName
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class FirstName extends Text
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('firstName')
            ->setOption('label', 'First Name')
            ->setAttribute('placeholder', 'First Name');
    }
}
