<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Select;

/**
 * Class for Select Element Role
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class SelectRole extends Select
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('roleId')
            ->setOption('label', 'Select Role');
    }
}
