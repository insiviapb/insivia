<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Checkbox;

/**
 * Class for Dynamic Checkbox
 *
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
class CheckDesc extends Checkbox
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct($name);
        $this->setName($name)
             ->setOption('label', 'CheckBox')
             ->setAttribute('id', $name);
    }
}
