<?php
/**
 * Created by PhpStorm.
 * User: adaloso
 * Date: 6/28/16
 * Time: 9:15 PM
 */
namespace Insivia\Form\Element;

use Zend\Form\Element\Select;

/**
 * Class for Select Element Item
 */
class SelectItem extends Select
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName($name)
            ->setOption('label', 'Select a ' . ucfirst($name));
    }
}
