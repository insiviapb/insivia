<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Text;

/**
 * Class for Text Element lastName
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class LastName extends Text
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('lastName')
            ->setOption('label', 'Last Name')
            ->setAttribute('placeholder', 'Last Name');
    }
}
