<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Email as FormElementEmail;

/**
 * Class for Text Element lastName
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class Email extends FormElementEmail
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName('email')
            ->setOption('label', 'Email')
            ->setAttribute('placeholder', 'Email');
    }
}
