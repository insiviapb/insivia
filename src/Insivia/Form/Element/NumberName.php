<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Number;

/**
 * Class for Number Input Element NumberName
 *
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
class NumberName extends Number
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct($name);
        $this->setName($name)
            ->setOption('label', 'Number Name');
        
        foreach($options as $key => $value) {
            $this->setAttribute($key, $value);
        }
        
    }
}
