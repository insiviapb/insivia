<?php
/**
 * Created by PhpStorm.
 * User: adaloso
 * Date: 6/27/16
 * Time: 8:31 PM
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Hidden;

/**
 * Class for Hidden Element
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class HiddenName extends Hidden
{
    public function __construct($name = null, $options = null)
    {
        parent::__construct();
        $this->setName($name);
    }
}
