<?php
/**
 * @copyright Copyright (c) 2015-2016
 */

namespace Insivia\Form\Element;

use Zend\Form\Element\Textarea;

/**
 * Class for Dynamic Text Element Description
 *
 * @author Oliver Pasigna <opasigna@insivia.com>
 */
class TextDesc extends Textarea
{
    public function __construct($name = null, $textDesc = null, $options = null)
    {
        parent::__construct();
        $this->setName(!empty($name) ? $name : "Text Description")
            ->setOption('label', 'Text Description')
            ->setAttribute('placeholder', (!empty($textDesc) ? $textDesc :'Text Description'));
        
        if ($options != null) {
            foreach ($options as $key => $val) {
                $this->setAttribute($key, $val);
            }
        }
    }
}
