<?php
namespace Insivia\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Insivia\Controller\RedirectCallback;

/**
 * RedirectCallback Factory
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class RedirectCallbackFactory implements FactoryInterface
{
    /**
     * Create a service for DoctrineObject Hydrator
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $router = $serviceLocator->get('Router');
        
        /* @var Application $application */
        $application = $serviceLocator->get('Application');
        
        /* @var ModuleOptions $options */
        $options = $serviceLocator->get('zfcuser_module_options');
        
        return new RedirectCallback($application, $router, $options);
//         $feedMapper = new FeedMapper();
//         $feedMapper->setEntityManager($serviceLocator->get('Doctrine\\ORM\\EntityManager'));
//         return $feedMapper;
    }
}
