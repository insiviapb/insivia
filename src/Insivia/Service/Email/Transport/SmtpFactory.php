<?php
namespace Insivia\Service\Email\Transport;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

/**
 * Mail Transport Object
 *
 * @author Dolly Aswin <dolly.aswin@gmail.com>
 */
class SmtpFactory implements FactoryInterface
{
    /**
     * Create a service for Registration Email
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config  = $serviceLocator->get('Config')['email']['transport']['smtp'];
        $transport = new SmtpTransport();
        $options   = new SmtpOptions($config['options']);
        $transport->setOptions($options);
        
        return $transport;
    }
}
