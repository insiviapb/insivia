<?php

namespace Insivia\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Lisyn\Mapper\MapperInterface;

class Role implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{
    use ServiceLocatorAwareTrait;

    use EventManagerAwareTrait;
    
    /**
     * @var MapperInterface
     */
    protected $authLogMapper;
    
    /**
     * @var string
     */
    protected $currentRole;
    
    /**
     * @var string
     */
    protected $publisherCurrentRole;
    
    /**
     * @var \Lisyn\Entity\Publisher
     */
    protected $publisher;

    /**
     * Insert AuthLog
     *
     * @param Lisyn\Entity\User $user
     * @param string            $role
     * @param DateTime|null     $loginTime
     * 
     * @return Lisyn\Entity\AuthLog
     * @throws Exception\InvalidArgumentException
     */
    public function createAuthLog($user, $role, $loginTime = null)
    {
        $authLogObject = new \Lisyn\Entity\AuthLog();
        if ($loginTime === null) {
            $loginTime = new \DateTime();
        }
        
        $data = array(
            'user' => $user,
            'role' => $role,
            'loginTime' => $loginTime
        );
        $authLog = $this->getAuthLogHydrator()->hydrate($data, $authLogObject);
        $this->getEventManager()->trigger(__FUNCTION__, $this, array('authLog' => $authLog, 'data' => $data));
        $this->getAuthLogMapper()->insert($authLog);
        $this->getEventManager()->trigger(
            __FUNCTION__.'.post',
            $this,
            array('authLog' => $authLog, 'data' => $data)
        );
        return $authLog;
    }
    
    /**
     * Set Logout Current AuthLog
     *
     * @param Lisyn\Entity\User $user
     * @param DateTime|null     $logoutTime
     *
     * @return Lisyn\Entity\AuthLog
     * @throws Exception\InvalidArgumentException
     */
    public function setLogoutAuthLog($user, $logoutTime = null)
    {
        if ($logoutTime === null) {
            $logoutTime = new \DateTime();
        }
        
        $authLogObject = $this->getAuthLogMapper()->getLatestRole($user);
        $data = array(
            'logoutTime' => $logoutTime
        );
        $authLog = $this->getAuthLogHydrator()->hydrate($data, $authLogObject);
        $this->getEventManager()->trigger(__FUNCTION__, $this, array('authLog' => $authLog, 'data' => $data));
        $this->getAuthLogMapper()->update($authLog);
        $this->getEventManager()->trigger(
            __FUNCTION__.'.post',
            $this,
            array('authLog' => $authLog, 'data' => $data)
        );
        return $authLog;
    }
    
    /**
     * Get Current Role
     * 
     * @return string
     */
    public function getCurrentRole()
    {
        if ($this->currentRole === null) {
            $zfcUserService = $this->getServiceLocator()->get('zfcuser_auth_service');
            $identity = $zfcUserService->getIdentity();
           
            if ($identity === null) {
                return;
            }
            
            $authLogMapper = $this->getServiceLocator()->get('lisyn.mapper.authLog');
            $role = $authLogMapper->getLatestRole($identity);
            $this->currentRole = $role->getRole();
        }
        
        return $this->currentRole;
    }
    
    /**
     * Get Publisher Current Role
     * @return string
     */
    public function getPublisherCurrentRole()
    {
        if ($this->publisherCurrentRole === null) {
            $zfcUserAuthService = $this->getServiceLocator()->get('zfcuser_auth_service');
            $identity = $zfcUserAuthService->getIdentity();
           
            if ($identity === null) {
                return;
            }
            $this->publisherCurrentRole = $identity->getPublisher()->getPublisherRole()->getId();
        }
        
        return $this->publisherCurrentRole;
    }

    /**
     * Set Current Role
     * 
     * @var string $role
     */
    public function setCurrentRole($role)
    {
        $this->currentRole = $role;
    }
    
    /**
     * Get Publisher
     * 
     * @return \Lisyn\Entity\Publisher
     */
    public function getPublisher()
    {
        if ($this->publisher !== null) {
            return $this->publisher;
        }
        
        $zfcUserAuthService = $this->getServiceLocator()->get('zfcuser_auth_service');
        $identity = $zfcUserAuthService->getIdentity();
        
        
        if ($identity !== null) {
            $userMapper = $this->getServiceLocator()->get('lisyn.mapper.user');
            $user = $userMapper->fetchOne($identity);
            if ($user->getPublisher()) {
                if ($user->getPublisher()->getParentPublisher() == 0) {
                    $this->publisher = $user->getPublisher();
                } else {
                    $publisherMapper  = $this->getServiceLocator()->get('lisyn.mapper.publisher');
                    $this->publisher  = $publisherMapper->fetchOne($user->getPublisher()->getParentPublisher());
                }
            }
        }
            
        return $this->publisher;
        
    }
    
    public function setPublisher(\Lisyn\Entity\Publisher $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * Get AuthLog Mapper
     *
     * @return MapperInterface
     */
    public function getAuthLogMapper()
    {
        if (null === $this->authLogMapper) {
            $this->authLogMapper = $this->getServiceLocator()->get('lisyn.mapper.authLog');
        }
        return $this->authLogMapper;
    }

    /**
     * Set AuthLogMapper
     *
     * @param  MapperInterface $authLogMapper
     * 
     * @return AuthLogMapper 
     */
    public function setAuthLogMapper(\Lisyn\Mapper\MapperInterface $authLogMapper)
    {
        $this->authLogMapper = $authLogMapper;
        return $this;
    }
    
    /**
     * Get Consumer Hydrator
     */
    public function getAuthLogHydrator()
    {
        $hydratorManager = $this->getServiceLocator()->get('HydratorManager');
        return $hydratorManager->get('lisyn.entity.authLogHydrator');
    }
}
