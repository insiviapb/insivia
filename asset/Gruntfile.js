module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    pkg: grunt.file.readJSON('package.json'),
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            bootstrap: {
                options: {
                  compress: true,
                  yuicompress: true,
                  optimization: 2
                },
                files: {
                    'css/voice-talent.min.css': [
                        'css/less/voice-talent.less'
                    ],
                }
            }
        },
        uglify: {
            consumer : {
                files : {
                    'js/min/consumer/consumer.min.js': [
                        'js/consumer/Model/addplaylist.js',
                        'js/consumer/Model/userfeedlist.js',
                        'js/consumer/Model/profiledetails.js',
                        'js/consumer/Model/publicfeedlist.js',
                        'js/consumer/Model/singlefeedlist.js',
                        'js/consumer/Model/topfeedlist.js',
                        'js/consumer/Model/topfeedlistbycategory.js',
                        'js/consumer/Model/searcharticle.js',
                        'js/consumer/View/mainnav.js',
                        'js/consumer/View/settingsnav.js',
                        'js/consumer/View/mainpage.js',
                        'js/consumer/View/feedlist.js',
                        'js/consumer/View/singlefeedslist.js',
                        'js/consumer/View/player.js',
                        'js/consumer/View/browsefeedlist.js',
                        'js/consumer/View/browsefeedscategorylist.js',
                        'js/consumer/View/searchplayer.js',
                        'js/consumer/consumer.js',
                        'js/consumer/fb.js'
                    ],
                    'js/min/addfile.min.js' : [
                        'js/addfile.js'
                    ],
                    'js/min/jquery.mtz.monthpicker.min.js' : [
                        'js/jquery.mtz.monthpicker.js'
                    ]
                }
            },
            voicetalent : {
                files : {
                    'js/min/voicetalent.min.js' : [
                        'js/voicetalent/Model/ongoing-article.js',
                        'js/voicetalent/Model/voice-talent-search.js',
                        'js/voicetalent/Model/add-publisher-voicetalent.js',
                        'js/voicetalent/Model/remove-publisher-voicetalent.js',
                        'js/voicetalent/Model/accept-job.js',
                        'js/voicetalent/Model/decline-job.js',
                        'js/voicetalent/Model/upload-audio.js',
                        'js/voicetalent/Model/cancel-audio.js',
                        'js/voicetalent/Model/delete-accent.js',
                        'js/voicetalent/Model/delete-language.js',
                        'js/voicetalent/Model/process-pricing.js',
                        'js/voicetalent/View/vt-search.js',
                        'js/voicetalent/View/pub-dashboard.js',
                        'js/voicetalent/View/publisher-vt-profile.js',
                        'js/voicetalent/View/vt-article-details.js',
                        'js/voicetalent/View/voice-talent-profile.js',
                        'js/voicetalent/View/getting-started.js',
                        'js/voicetalent/View/gt-update-profile.js',
                        'js/voicetalent/View/gt-upload-photo.js',
                        'js/voicetalent/View/gt-setup-pricing.js',
                        'js/voicetalent/voicetalent.js'
                    ],
                }
            },
            publisher : {
                files : {
                    'js/min/scripts.min.js' : [
                        'js/scripts.js',
                        'js/publisher/all.js',
                        'js/publisher/dashboard.js',
                        'js/publisher/ads.js',
                        'js/publisher/article.js',
                        'js/publisher/profile.js',
                        'js/min/voicetalent.min.js',
                    ],
                    'js/min/consumer-xpull.min.js' : [
                        'js/addfile.js',
                        'js/xpull.js'
                    ]
                }
            }
        },        
        cssmin: {
            consumer: {
                files: {
                    'css/min/consumer-xpull.min.css': [
                        'css/xpull.css',
                        'css/consumer.css'
                    ],
                    'css/min/override-consumer.min.css' : [
                        'css/consumer.css',
                        'css/override-consumer.css'
                    ],
                    'css/min/magicsuggest.min.css' : [
                        'css/magicsuggest.css'
                    ],
                    'css/min/embeded.min.css' : [
                        'css/embeded.css'
                    ]
                }
            },
            publisher: {
                files : {
                    'css/min/interface.min.css' : [
                        'css/interface.css',
                        'css/getting-started.css',
                        'css/voice-talent.min.css'
                    ]
                }
            }
        },
        watch: {
            consumer: {
                files: ['js/consumer/**/*.js','js/*.js', 'css/*.css'],
                tasks: ['uglify','cssmin'],
            },
            
            publisher: {
                files: ['js/publisher/**/*.js','js/*.js', 'css/*.css'],
                tasks: ['uglify'],
            },
            voicetalent: {
                files: ['js/voicetalent/**/*.js','js/*.js', 'css/*.css'],
                tasks: ['uglify'],
            },
        },
    });
    
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    
    grunt.registerTask('default',['less', 'uglify', 'cssmin']);
}
