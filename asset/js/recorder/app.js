function restore() {
    $("#record, #live").removeClass("disabled");
    $("#pause").replaceWith('<a class="btn btn-success one" id="pause">Pause</a>');
    $(".one").addClass("disabled");
    Fr.voice.stop();
}
$(document).ready(function () {
    $(document).on("click", "#record:not(.disabled)", function () {
        
        $('#play').removeAttr('disabled');
        $('#pause').removeAttr('disabled');
        $('#stop').removeAttr('disabled');
        elem = $(this);
        Fr.voice.record($("#live").is(":checked"), function () {
            elem.addClass("disabled");
            $("#live").addClass("disabled");
            $(".one").removeClass("disabled");

            /**
             * The Waveform canvas
             */
            analyser = Fr.voice.context.createAnalyser();
            analyser.fftSize = 2048;
            analyser.minDecibels = -90;
            analyser.maxDecibels = -10;
            analyser.smoothingTimeConstant = 0.85;
            Fr.voice.input.connect(analyser);

            var bufferLength = analyser.frequencyBinCount;
            var dataArray = new Uint8Array(bufferLength);

            WIDTH = 500, HEIGHT = 200;
            canvasCtx = $("#level")[0].getContext("2d");
            canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

            function draw() {
                drawVisual = requestAnimationFrame(draw);
                analyser.getByteTimeDomainData(dataArray);
                canvasCtx.fillStyle = 'rgb(200, 200, 200)';
                canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
                canvasCtx.lineWidth = 2;
                canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

                canvasCtx.beginPath();
                var sliceWidth = WIDTH * 1.0 / bufferLength;
                var x = 0;
                for (var i = 0; i < bufferLength; i++) {
                    var v = dataArray[i] / 128.0;
                    var y = v * HEIGHT / 2;

                    if (i === 0) {
                        canvasCtx.moveTo(x, y);
                    } else {
                        canvasCtx.lineTo(x, y);
                    }

                    x += sliceWidth;
                }
                canvasCtx.lineTo(WIDTH, HEIGHT / 2);
                canvasCtx.stroke();
            }
            ;
            draw();
        });
    });

    $(document).on("click", "#pause:not(.disabled)", function () {
        if ($(this).hasClass("resume")) {
            Fr.voice.resume();
            $(this).replaceWith('<a class="btn btn-success one" id="pause">Pause</a>');
        } else {
            Fr.voice.pause();
            $(this).replaceWith('<a class="btn btn-success one resume" id="pause">Resume</a>');
        }
    });

    $(document).on("click", "#stop:not(.disabled)", function () {
        restore();
    });

    $(document).on("click", "#play:not(.disabled)", function () {
        $('#play').attr('disabled', 'disabled');
        $('#pause').attr('disabled', 'disabled');
        $('#stop').attr('disabled', 'disabled');
        $('#record').removeClass('disabled');
        Fr.voice.export(function (url) {
            $("#audio").attr("src", url);
            $("#audio")[0].play();
        }, "URL");
    });

    $(document).on("click", "#recordAudio #saveAudioRecord:not(.disabled)", function () {
        Fr.voice.export(function (blob) {
            var formData = new FormData();
            formData.append('uuid', $('#recordAudio #articleUuid').val());
            formData.append('file', blob);
            $.ajax({
                url: "/publisher/article/save-audio-record",
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (url) {
                    location.reload();
                }
            });
        }, "blob");
        restore();
    });
    
    $(document).on("click", "#recordAdsAudio #saveAudioRecord:not(.disabled)", function () {
        Fr.voice.export(function (blob) {
            var formData = new FormData();
            formData.append('uuid', $('#recordAdsAudio #adsUuid').val());
            formData.append('file', blob);
            $.ajax({
                url: "/publisher/ads/save-audio-record",
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (url) {
                    location.reload();
                }
            });
        }, "blob");
        restore();
    });
    
    $(document).on("click", "#recordVTAudio #saveAudioRecord:not(.disabled)", function () {
        Fr.voice.export(function (blob) {
            var formData = new FormData();
            formData.append('uuid', $('#recordVTAudio #articleUuid').val());
            formData.append('assignedArticle', $('#assignArticle').val());
            formData.append('file', blob);
            $.ajax({
                url: "/voice-talent/process/save-audio-record",
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.upload) {
                        location.reload();
                    } else {
                        alert('An error occured. Please try again later.');
                    }
                }
            });
        }, "blob");
        restore();
    });
});
