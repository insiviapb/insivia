$(document).ready(function(){
    
    /* 
     * PublisherConfig
     */
    var PublisherConfig = PublisherConfig || {};
    PublisherConfig = {
        errorMessage : "An error occured. Please try again later.",
        init: function(){
            $(".submitConfiguration").click(function() {

                _this = PublisherConfig;

                var publisherId = $('#publisher_id').val();
                var formData    = $('#buttonConfiguration').serialize();
                
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: "/publisher/settings/code/save/"+publisherId,
                    success: function (data) {
                        
                        if (data.success) {
                            alert(data.message);
                            location.reload();
                        } else {
                            alert(_this.errorMessage);
                        }
                    },
                    error: function () {
                        alert(_this.errorMessage);
                    },
                    data: formData
                });
            });
        },
    }
    PublisherConfig.init();
    
});