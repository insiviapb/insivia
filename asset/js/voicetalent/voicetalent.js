var vtsearch;
var pubVTSearch;
var pubVtProfile;
var vtArticleDetails;
var gettingStarted;
var gtUpdateProfile;
var gtUploadPhoto;
var gtSetupPricing;
var VoiceTalent = Backbone.View.extend({
    initialize: function(data) {
        // search article
        this.vtSearch          = data.vtSearch;
        this.ongoingArticle    = data.ongoingArticle;
        //search voice talent
        this.pubVTSearch       = data.pubVTSearch;
        this.voiceTalentSearch = data.voiceTalentSearch;
        // publisher - voice-talent profile
        this.pubVTProfile      = data.pubVTProfile;
        // voice talent profile
        this.voiceTalentProfile = data.voiceTalentProfile;
        this.addPublisherVT    = data.addPublisherVT;
        this.removePublisherVT = data.removePublisherVT;
        // voice talent article details
        this.vtArticleDetails  = data.vtArticleDetails;
        this.acceptJob         = data.acceptJob;
        this.declineJob        = data.declineJob;
        this.uploadAudio       = data.uploadAudio;
        this.cancelAudio       = data.cancelAudio;
        this.deleteAccent      = data.deleteAccent;
        // voice talent getting started
        this.gettingStarted    = data.gettingStarted;
        // voice talent getting started update profile
        this.gtUpdateProfile   = data.gtUpdateProfile;
        // voice talent getting started upload photo
        this.gtUploadPhoto     = data.gtUploadPhoto;
        // voice talent getting started setup pricing
        this.gtSetupPricing    = data.gtSetupPricing;
        this.processPricing    = data.processPricing;
                
        if ( $('.vtdashboards').length > 0 ) {
            this.renderVTSearch();
        }
        if ( $('.publisher-vt-search').length > 0 ) {
            this.renderPublisherVTSearch();
        }
        if ( $('.publisher-vt-profile').length > 0 ) {
            this.renderPublisherVTProfile();
        }
        if ( $('.vt-article-details').length > 0 ) {
            this.renderVTArticleDetails();
        }
        if ( $('.voice-talent-profile').length > 0 ) {
            this.renderVoiceTalentProfile();
        }
        if ( $('.getting-started').length > 0 ) {
            this.renderGettingStarted();
        }
        if ( $('.gt-update-profile').length > 0 ) {
            this.renderGTUpdateProfile();
        }
        if ( $('.gt-upload-photo').length > 0 ) {
            this.renderGTUploadPhoto();
        }
        if ( $('.gt-setup-pricing').length > 0 ) {
            this.renderGTSetupPricing();
        }
    },
    renderVTSearch: function() {
        vtsearch = new this.vtSearch({
            el                : $('.vtdashboards'),
            ongoingArticle    : this.ongoingArticle,
            listTemplate      : $('#vt-search-article-list').html(),
            noArticleTemplate : $('#vt-no-article-found').html()
        });
    },
    renderPublisherVTSearch: function() {
        pubVTSearch = new this.pubVTSearch({
            el                : $('.publisher-vt-search'),
            voiceTalentSearch : this.voiceTalentSearch,
            listTemplate      : $('#vt-search-voicetalent').html(),
            noArticleTemplate : $('#vt-no-voicetalent-found').html()
        });
    },
    renderPublisherVTProfile: function() {
        pubVtProfile = new this.pubVTProfile({
            el                : $('.publisher-vt-profile'),
            addPublisherVT    : this.addPublisherVT,
            removePublisherVT : this.removePublisherVT,
        });
    },
    renderVTArticleDetails: function() {
        vtArticleDetails = new this.vtArticleDetails({
            el               : $('.vt-article-details'),
            acceptJob        : this.acceptJob,
            declineJob       : this.declineJob,
            uploadAudio      : this.uploadAudio,
            cancelAudio      : this.cancelAudio
        });
    },
    renderVoiceTalentProfile: function() {
        voiceTalentProfile  = new this.voiceTalentProfile({
            el                : $('.voice-talent-profile'),
            deleteAccent      : this.deleteAccent
        });
    },
    renderGettingStarted: function() {
        getttingStarted = new this.gettingStarted({
            el            : $('.getting-started'),
        });
    },
    renderGTUpdateProfile: function() {
        gtUpdateProfile = new this.gtUpdateProfile({
            el            : $('.gt-update-profile'),
        });
    },
    renderGTUploadPhoto: function() {
        gtUploadPhoto   = new this.gtUploadPhoto({
            el            : $('.gt-upload-photo')
        });
    },
    renderGTSetupPricing: function() {
        gtSetupPricing = new this.gtSetupPricing({
            el             : $('.gt-setup-pricing'),
            processPricing : this.processPricing
        });
    },
});

var voiceTalentPage = new VoiceTalent({
    vtSearch          : VTSearch,
    pubVTSearch       : PubVTSearch,
    pubVTProfile      : PubVTProfile,
    vtArticleDetails  : VTArticleDetails,
    voiceTalentProfile: VoiceTalentProfile,
    gettingStarted    : GettingStarted,
    gtUpdateProfile   : GTUpdateProfile,
    gtUploadPhoto     : GTUploadPhoto,
    gtSetupPricing    : GTSetupPricing,
    ongoingArticle    : new OngoingArticle(),
    voiceTalentSearch : new VoiceTalentSearch(),
    addPublisherVT    : new AddPublisherVoiceTalent(),
    removePublisherVT : new RemovePublisherVoiceTalent(),
    acceptJob         : new AcceptJob(),
    declineJob        : new DeclineJob(),
    uploadAudio       : new UploadAudio(),
    cancelAudio       : new CancelAudio(),
    deleteAccent      : new DeleteAccent(),
    processPricing    : new ProcessPricing()
});