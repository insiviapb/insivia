GettingStarted = Backbone.View.extend({
    events :{
        'click #btn-upload' : 'uploadFile',
        'click .btn-next-profile' : 'loadNextPage'
    },
    initialize: function(data){
        this.processSample = data.processSample;
        this.initializeUploader();
        
        if (Object.keys(samples).length == 5) {
            this.$el.find('.btn-next-profile').removeClass('hide');
        }
    },
    uploadFile: function(el) {
        this.$el.find(el.currentTarget).siblings('#audioFile').click();
    },
    initializeUploader: function() {
        $('.article-upload #audioFile').fileupload({
            url: '/voice-talent/getting-started/upload-sample/process-sample',
            dataType: 'json',
            change: function (e, data) {
                $(this).closest('.audio-file').find('#progress .progress-bar').css('width', '0%');
            },
            done: function (e, data) {
                if (data.result.success) {
                    samples[data.result.id] = data.result.data;
                    $(this).closest('.audio-file').find('#files').prepend('<span>Already Uploaded.</span>');
                    $(this).closest('.audio-file').find('#files #buzzer').removeClass('hide');
                    $(this).closest('.audio-file').find('#files #buzzer').html('<source src="'+data.result.data+'"></source>');
                    if (Object.keys(samples).length == 5) {
                        $('.btn-next-profile').removeClass('hide');
                    }
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                 $(this).closest('.audio-file').find('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

    },
    loadNextPage: function() {
        window.location = '/voice-talent/getting-started/update-profile';
        return false;
    }
});