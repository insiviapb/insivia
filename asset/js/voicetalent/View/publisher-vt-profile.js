PubVTProfile = Backbone.View.extend({
    events :{
        'click .not-added-vt'     : 'addVT',
        'click .already-added-vt' : 'removeVT'
    },
    initialize: function(data){
        this.addPublisherVT    = data.addPublisherVT;
        this.removePublisherVT = data.removePublisherVT;
        this.listenTo(this.addPublisherVT,    'change', this.renderAdd);
        this.listenTo(this.removePublisherVT, 'change', this.renderRemove);
    },
    addVT: function() {
        this.processAdd = this.addPublisherVT.fetch({
            data: {uuid : voicetalentId},
            type: 'POST',
            beforeSend: function () {
                if (typeof pubVtProfile.processAdd != 'undefined' && pubVtProfile.processAdd != null) {
                    pubVtProfile.processAdd.abort();
                }
            },
        });
    },
    removeVT: function() {
        this.processRemove = this.removePublisherVT.fetch({
            data: {uuid : voicetalentId},
            type: 'POST',
            beforeSend: function () {
                if (typeof pubVtProfile.processRemove != 'undefined' && pubVtProfile.processRemove != null) {
                    pubVtProfile.processRemove.abort();
                }
            },
        });
    },
    renderAdd: function() {
        var data = this.addPublisherVT.toJSON();
        if (data.success) {
            location.reload();
        } else {
            alert('An error occured. Please reload and try again.');
        }
    },
    renderRemove: function() {
        var data = this.removePublisherVT.toJSON();
        if (data.success) {
            location.reload();
        } else {
            alert('An error occured. Please reload and try again.');
        }
    },
});