GTUpdateProfile = Backbone.View.extend({
    events :{
        'click #backToUpload' : 'backToUpload',
        'click #saveProfile'  : 'saveProfile'
    },
    initialize: function(data){
        this.initializeComboBox();
    },
    initializeComboBox: function() {
        // for accents
        var msAccent = $('#msAccent').magicSuggest({
            placeholder: 'Type some accent or select an accent',
            maxSelection: null,
            allowFreeEntries: false,
            noSuggestionText: "No result matching the term <i>{{query}}</i>",
            data: accents
        });
        msAccent.setValue(selectedAccents);

        // for languages
        var msLang = $('#msLanguage').magicSuggest({
            placeholder: 'Type some language or select an language',
            maxSelection: null,
            allowFreeEntries: false,
            noSuggestionText: "No result matching the term <i>{{query}}</i>",
            data: languages
        });
        msLang.setValue(selectedLanguages);
        
        // for tags
        var msTags = $('#msTags').magicSuggest({
            placeholder: 'Type some tags or select a tags',
            maxSelection: null,
            allowFreeEntries: true,
            data: tags
        });
        msTags.setValue(selectedTags);

    },
    backToUpload: function() {
        window.location = '/voice-talent/getting-started';
        return false;
    },
    saveProfile: function() {
        $('#frmVtProfile').submit();
    }
});