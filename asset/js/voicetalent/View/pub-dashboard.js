PubVTSearch = Backbone.View.extend({
    events :{
        'click #btn-search'     : 'getVoiceTalents',
        'keypress #main-search' : 'searchOnEnter',
        'click a[search]'       : 'searchFilter'
    },
    initialize: function(data){
        this.voiceTalentSearch    = data.voiceTalentSearch;
        this.listTemplate      = data.listTemplate;
        this.noArticleTemplate = data.noArticleTemplate;
        this.listenTo(this.voiceTalentSearch, 'change', this.render);
    },
    render: function() {
        var data              = this.voiceTalentSearch.toJSON(),
            listTemplate      = _.template(this.listTemplate),
            noArticleTemplate = _.template(this.noArticleTemplate),
            searchContainer   =  this.$el.find('.talent-list');
            
        if (typeof data.voicetalent != 'undefined' && Object.keys(data.voicetalent).length > 0) {
            //reset value
            searchContainer.html('');
            $.each(data.voicetalent, function(k, v) {
                searchContainer.append(listTemplate(v));
            });
        } else {
            searchContainer.html('');
            searchContainer.append(noArticleTemplate({}));
        }
    },
    searchOnEnter: function(e) {
        if (e.keyCode != 13) return;
        this.getVoiceTalents(e);
    },
    getVoiceTalents: function() {
        
        var filters = {};
        var keyword = this.$el.find('#main-search').val();
        $('a[search].search-active').each(function(){
            if ($(this).html() != 'All') {
                filters[$(this).attr('search')] = $(this).html();
            }
        });
        if ($.trim(keyword).length != 0) {
            filters['keyword'] = keyword;
        }
        this.searchVoiceTalent = this.voiceTalentSearch.fetch({
            type: 'POST',
            data: filters,
            beforeSend: function () {
                if (typeof pubVTSearch.searchVoiceTalent != 'undefined' && pubVTSearch.searchVoiceTalent != null) {
                    pubVTSearch.searchVoiceTalent.abort();
                }
            },
        });
    },
    searchFilter: function(el) {
        this.$el.find('a[search='+this.$el.find(el.currentTarget).attr('search')+']').removeClass('search-active');
        this.$el.find(el.currentTarget).addClass('search-active');
        this.getVoiceTalents();
    },
});