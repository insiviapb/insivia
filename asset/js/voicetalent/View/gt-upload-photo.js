GTUploadPhoto = Backbone.View.extend({
    events :{
        'click #backtoprofile'    : 'backtoProfile',
        'click #proceedtopricing' : 'proceedToPricing',
        'click #btn-upload'       : 'uploadPhoto'
    },
    initialize: function(data){
        this.initializeUploader();
    },
    uploadPhoto: function() {
        this.$el.find('#audioImage').click();
    },
    initializeUploader:function() {
        $('.profile-big').fileupload({
            url: '/voice-talent/process/save-profile-photo',
            dataType: 'json',
            change: function (e, data) {
                $('.profile-big #progress .progress-bar').css('width', '0%');
            },
            done: function (e, data) {
                if (data.result.success) {
                    $('.profile-big img').attr('src', data.result.data);
                    $('.profile-big #files').html('<i class="fa fa-check"></i>  Profile Photo successfully saved.');
                }
                else {
                    $('.profile-big #files').html(data.result.data);
                    $('.profile-big input[name="fileName"]').val(data.result.data);
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.profile-big #progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        
        
        $('.talent-img').click(function(){
             $('.profile-big #audioImage').click()
        });
    },
    backToProfile: function() {
        window.location = '/voice-talent/getting-started/update-profile';
        return false;
    },
    proceedToPricing: function() {
        window.location = '/voice-talent/getting-started/setup-pricing';
        return false;
    }
});