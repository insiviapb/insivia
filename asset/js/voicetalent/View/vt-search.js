VTSearch = Backbone.View.extend({
    events :{
        'click #btn-search'     : 'getOngoingArticles',
        'keypress #main-search' : 'searchOnEnter'
    },
    initialize: function(data){
        this.ongoingArticle    = data.ongoingArticle;
        this.listTemplate      = data.listTemplate;
        this.noArticleTemplate = data.noArticleTemplate;
        this.listenTo(this.ongoingArticle, 'change', this.render);
    },
    render: function() {
        var data              = this.ongoingArticle.toJSON(),
            listTemplate      = _.template(this.listTemplate),
            noArticleTemplate = _.template(this.noArticleTemplate),
            searchContainer   =  this.$el.find('.pending-article-list');
            
        if (typeof data.article != 'undefined' && Object.keys(data.article).length > 0) {
            //reset value
            searchContainer.html('');
            $.each(data.article, function(k, v) {
                searchContainer.append(listTemplate(v));
            });
        } else {
            searchContainer.html('');
            searchContainer.append(noArticleTemplate({}));
        }
    },
    searchOnEnter: function(e) {
        if (e.keyCode != 13) return;
        this.getOngoingArticles(e);
    },
    getOngoingArticles: function() {
        var keyword = this.$el.find('#main-search').val();
        this.searchArticle = this.ongoingArticle.fetch({
            type: 'POST',
            data: {keyword : keyword},
            beforeSend: function () {
                if (typeof vtsearch.searchArticle != 'undefined' && vtsearch.searchArticle != null) {
                    vtsearch.searchArticle.abort();
                }
            },
        });
    }
});

