VoiceTalentProfile = Backbone.View.extend({
    events: {
        'click #saveProfile'   : 'submitVTProfile',
        'click #backtoprofile' : 'goToProfileView'
    },

    initialize: function() {
        this.loadMagicSuggest();
        this.initializeUploader();
    },
    initializeUploader:function() {
        $('.profile-big').fileupload({
            url: '/voice-talent/process/save-profile-photo',
            dataType: 'json',
            change: function (e, data) {
                $('.profile-big #progress .progress-bar').css('width', '0%');
            },
            done: function (e, data) {
                if (data.result.success) {
                    $('profile-big a').attr('src', data.result.data);
                    $('.talent-img').attr('style', 'background-image:url('+data.result.data+ ')');
                }
                else {
                    $('.profile-big #files').html(data.result.data);
                    $('.profile-big input[name="fileName"]').val(data.result.data);
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.profile-big #progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        
        
        $('.talent-img').click(function(){
             $('.profile-big #audioImage').click()
        });
    },
    loadMagicSuggest: function() {
        // for accents
        var msAccent = $('#msAccent').magicSuggest({
            placeholder: 'Type some accent or select an accent',
            maxSelection: null,
            allowFreeEntries: false,
            noSuggestionText: "No result matching the term <i>{{query}}</i>",
            data: accents
        });
        msAccent.setValue(selectedAccents);

        // for languages
        var msLang = $('#msLanguage').magicSuggest({
            placeholder: 'Type some language or select an language',
            maxSelection: null,
            allowFreeEntries: false,
            noSuggestionText: "No result matching the term <i>{{query}}</i>",
            data: languages
        });
        msLang.setValue(selectedLanguages);
        
        // for tags
        var msTags = $('#msTags').magicSuggest({
            placeholder: 'Type some tags or select a tags',
            maxSelection: null,
            allowFreeEntries: true,
            noSuggestionText: "No result matching the term <i>{{query}}</i>",
            data: tags
        });
        msTags.setValue(selectedTags);
    },
    submitVTProfile: function() {
        $('#frmVtProfile').submit();
    },
    goToProfileView: function() {
        window.location.href = viewProfileUrl;
        return false;
    }
});
