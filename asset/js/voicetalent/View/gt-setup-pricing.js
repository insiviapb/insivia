GTSetupPricing = Backbone.View.extend({
    events :{
        'click #backtoUploadPhoto' : 'backtoUploadPhoto',
        'click #submit'            : 'submit',
        'click #agree'             : 'agree',
        'click #disagree'          : 'disagree',
        'keyup #audio-price'       : 'keyupAudioPrice'
    },
    initialize: function(data){
        this.processPricing = data.processPricing;
        this.listenTo(this.processPricing, 'change', this.checkResult);
        this.$el.find('#disagree').prop('checked', true);
    },
    backtoUploadPhoto: function() {
        window.location = '/voice-talent/getting-started/upload-photo';
        return false;
    },
    agree: function() {
        this.$el.find('#submit').attr('disabled', 'disabled');
        if ( $.trim(this.$el.find('#audio-price').val()).length > 0 ) {
            this.$el.find('#submit').removeAttr('disabled');
        }
    },
    disagree: function() {
        this.$el.find('#submit').attr('disabled', 'disabled');
    },
    keyupAudioPrice: function(el) {
        var valid = /^\d{0,4}(\.\d{0,2})?$/.test(this.$el.find(el.currentTarget).val()),
        val = this.$el.find(el.currentTarget).val();
    
        if(!valid){
            this.$el.find(el.currentTarget).val(val.substring(0, val.length - 1));
        }
        
        this.$el.find('#submit').attr('disabled', 'disabled');
        var isChecked = this.$el.find('#agree').is(':checked');
        if ( isChecked ) {
            if ($.trim(this.$el.find(el.currentTarget).val()).length > 0 ) {
                this.$el.find('#submit').removeAttr('disabled');
            }
        }
    },
    submit: function() {
        this.$el.find('#loader').show();
        
        data = {
            isChecked  : this.$el.find('#agree').is(':checked'),
            audioPrice : this.$el.find('#audio-price').val()
        };
        
        this.processPriceDetails = this.processPricing.fetch({
            type: 'POST',
            data: data,
            beforeSend: function () {
                if (typeof gtSetupPricing.processPriceDetails != 'undefined' && gtSetupPricing.processPriceDetails != null) {
                    gtSetupPricing.processPriceDetails.abort();
                }
            }, 
        });
    },
    checkResult: function() {
        var pricing = this.processPricing.toJSON();
        if ( pricing.success ) {
            window.location = '/voice-talent';
        } else {
            alert('There is an error occured. Please check and try again.');
        }
    }
});