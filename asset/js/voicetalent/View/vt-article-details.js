VTArticleDetails = Backbone.View.extend({
    events :{
        'click .accept-job'        : 'acceptJob',
        'click .decline-job'       : 'declineJob',
        'click .upload-audio-btn'  : 'displayAudioUpload',
        'click #btn-upload'        : 'uploadFromComp',
        'click #uploadAudioButton' : 'submitAudio',
        'click .vt-cancel-audio'   : 'cancelAudio'
    },
    initialize: function(data){
        this.acceptJobData   = data.acceptJob;
        this.declineJobData  = data.declineJob;
        this.uploadAudioData = data.uploadAudio;
        this.cancelAudioData = data.cancelAudio;
        this.listenTo(this.acceptJobData,  'change', this.acceptJobResult);
        this.listenTo(this.declineJobData, 'change', this.declineJobResult);
        this.listenTo(this.uploadAudioData, 'change', this.uploadAudioResult);
        this.listenTo(this.cancelAudioData, 'change', this.cancelAudioResult);
        this.initializeUploader(this.declineJobData, 'change', this.declineJobResult);
    },
    initializeUploader: function() {
        $('.audio-file #audioFile').fileupload({
            url: '/voice-talent/process/file-handler',
            dataType: 'json',
            change: function (e, data) {
                $('.audio-file #progress .progress-bar').css('width', '0%');
            },
            done: function (e, data) {
                $('#loader').hide();
                $('.audio-file #files').html(data.result.data);
                $('.audio-file input[name="fileName"]').val(data.result.data);
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.audio-file #progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
                $('#loader').show();
            }
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

    },
    acceptJob: function() {
        this.processAccept = this.acceptJobData.fetch({
            data: {uuid : vtArticleId},
            type: 'POST',
            beforeSend: function () {
                if (typeof vtArticleDetails.processAccept != 'undefined' && vtArticleDetails.processAccept != null) {
                    vtArticleDetails.processAccept.abort();
                }
            },
        });
        $('#loader-accept').show();
    },
    declineJob: function() {
        this.processDecline = this.declineJobData.fetch({
            data: {uuid : vtArticleId},
            type: 'POST',
            beforeSend: function () {
                if (typeof vtArticleDetails.processDecline != 'undefined' && vtArticleDetails.processDecline != null) {
                    vtArticleDetails.processDecline.abort();
                }
            },
        });
        $('#loader-decline').show()
    },
    acceptJobResult: function() {
        $('#loader-accept').hide();
        var data = this.acceptJobData.toJSON();
        if (data.success) {
            location.reload();
        } else {
            alert('An error occured. Please reload and try again.');
        }
    },
    declineJobResult: function() {
        $('#loader-accept').hide();
        var data = this.declineJobData.toJSON();
        if (data.success) {
            location.reload();
        } else {
            alert('An error occured. Please reload and try again.');
        }
    },
    uploadAudioResult: function() {
        $('#loader').hide();
        var data = this.uploadAudioData.toJSON();
        if (data.upload) {
            location.reload();
        } else {
            alert('An error occured. Please reload and try again.');
        }
    },
    cancelAudioResult: function() {
        var data = this.cancelAudioData.toJSON();
        if (data.success) {
            location.reload();
        } else {
            alert('An error occured. Please reload and try again.');
        }
    },
    displayAudioUpload: function() {
        this.$el.find('#uploadAudio').modal('show');
    },
    uploadFromComp: function() {
        this.$el.find('#audioFile').click();
    },
    submitAudio: function() {
        $('#loader').show();
        var data   = this.$el.find('#uploadArticleFile').serialize();
            data  += '&files='+this.$el.find('#files').html();
            data  += '&assignedArticle='+vtArticleId;
        
        this.uploadAudioProcess = this.uploadAudioData.fetch({
            data: data,
            dataType: "json",
            type: 'POST',
            beforeSend: function () {
                if (typeof vtArticleDetails.uploadAudioProcess != 'undefined' && vtArticleDetails.uploadAudioProcess != null) {
                    vtArticleDetails.uploadAudioProcess.abort();
                }
            },
        })
    },
    cancelAudio: function(el) {
        var uuid = this.$el.find(el.currentTarget).data('uuid');
        this.processCancel = this.cancelAudioData.fetch({
            data: {uuid : uuid},
            type: 'POST',
            beforeSend: function () {
                if (typeof vtArticleDetails.processCancel != 'undefined' && vtArticleDetails.processDecline != null) {
                    vtArticleDetails.processCancel.abort();
                }
            },
        });
    }
});