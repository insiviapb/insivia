$(document).ready(function(){
	
    // Dashbaord
    if ( $('#dashboard').length > 0 ) {
            Dashboard.init();
    }

    // Ads
    if ( $('#ads').length > 0 ) {
            Ads.init();
    }
    if ( $('#manage-ad').length > 0 ) {
            Ads.manage();
    }
    if ( $('#ad-activity').length > 0 ) {
            Ads.activity();
    }

    // Articles
    if ( $('#articles').length > 0 || $('#archived-list.articles').length > 0) {
            Articles.init();
    }
    if ( $('#manage-article').length > 0 ) {
            Articles.manage();
    }
    if ( $('#article-activity').length > 0 ) {
            Articles.activity();
    }

    // Profile
    if ( $('#profile').length > 0 ) {
            Profile.init();
    }

    // All Pages
    All.init();

    // for responsiveness of navigation
    $( window ).resize(function() {
        if ( $( window ).width() >= 824) {
            $('.navbar-top .navbar-right').append($('.navbar-top .navbar-left .rnav'));
            $('.navbar-top .navbar-left .rnav').remove();
        } else {
            $('.navbar-top .navbar-left').append($('.navbar-top .navbar-right .rnav'));
            $('.navbar-top .navbar-right .rnav').remove();
        }
    });
    $(window).trigger('resize');
});

