$(document).ready(function(){
    
    if( typeof $('.lisyn-form').data('login') == "undefined") {
        $('.lisyn-form .form-group input').on('focus',
        function() {
            $(this).parent().css('border', '1px solid green');
            if($(this).val().length == 0) {
                var lbl = $(this).attr('placeholder');
                $(this).attr('placeholder','').css('height', '22px');
                $(this).prev('span').html(lbl).show();
            }
        }).on('focusout',
        function() {
            var lbl = $(this).prev('span').html();
            $(this).parent().css('border', '1px solid #ccc');
            if($(this).val().length > 0) {
                return false;
            }
            $(this).attr('placeholder', lbl).css('height', '42px');
            $(this).prev('span').html('').hide();
        });
    }

    
   $('a[name="feeds-list"]').click(function(){
       var feedId = $(this).attr('dt-feed-id');
       var fileId = $('input[name="fileUuid"]').val();
       $('input[name="feedId"]').val(feedId);
       $('input[name="fileDuration"]').val($('.duration').html());
       $('#AddThisFile').submit();
   });
    if ($('.login, .register').length > 0) {
       $('header #logo').remove();
    } else {
        $('header').show();
        $('header #logo').show();
    }
});

head.ready('backbone', function(){
    var Player = Backbone.View.extend({
        events: {
            'click .fa-play-circle': 'playAudio',
            'click .fa-pause-circle': 'pauseAudio',
        },
        initialize:function(data) {
            this.fileName = data.filename;
            this.render();
        },
        render: function() {
            this.audioFile = new Audio(this.fileName);
            this.audioFile.addEventListener("ended", function(){
                $('.fa-pause-circle').replaceWith('<i class="fa fa-play-circle"></i>');
            });
            this.audioFile.addEventListener('loadedmetadata', function() {
                $('.duration').html(singlePlayer.formatSeconds(singlePlayer.audioFile.duration));
            });
        },
        playAudio: function(el) {
            this.$el.find('.duration').html(this.formatSeconds(this.audioFile.duration));
            this.$el.find(el.currentTarget).removeClass('fa-play-circle').addClass('fa-pause-circle');
            this.audioFile.play();
        },
        pauseAudio: function(el) {
            this.$el.find(el.currentTarget).removeClass('fa-pause-circle').addClass('fa-play-circle');
            this.audioFile.pause();
        },
        formatSeconds: function(gTime) {
            time = Math.round(gTime);
            var minutes = Math.floor(time / 60);
            var seconds = time - minutes * 60;
            return String(this.pad(minutes,2)) +'.'+String(this.pad(seconds,2));
        },
        pad: function (str, max) {
            str = str.toString();
            return str.length < max ? this.pad("0" + str, max) : str;
        },
    });

    if ( $('#addPlayList').length > 0 ) {
        /**
        * Add MainPage
        */
        var MainPage = Backbone.View.extend({
            events :{
                'click #add-playlist'         : 'addPlayList',
                'click #close-modal-playlist' : 'closeModalPlayList',
                'click .close'                : 'closeModalPlayList'
            },
            initialize: function(data) {
                this.playList = data.playList;
                this.listenTo(this.playList, 'change', this.reloadPage);
            },
            addPlayList: function() {
                this.playList.fetch({
                    data: $('#frmAddPlaylist').serialize(),
                    type: 'POST',
                });
            },
            reloadPage: function() {
                location.reload();
            },
            closeModalPlayList: function() {
                this.$el.find('#txt-playlist').val('');
                this.$el.find('#txt-playlist-desc').val('');
                this.$el.find('#addPlayList').modal('hide');
            }
        });

        var addPlayList = Backbone.Model.extend({
            url : '/consumer/feeds/process/addPlaylist'
        });
        var mainPage = new MainPage({
            el : $('#addThisFilePage'),
            playList : new addPlayList,
        });
    }

    if ( $('.player-div').length > 0 ) {
        singlePlayer = new Player({
            filename : fileName,
            el       : '.player-div'
        });
    }
});
