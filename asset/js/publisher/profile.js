/* 
 * PROFILE
 */
var Profile = Profile || {};
Profile = {
    init: function(){

        if (typeof $('#signature').summernote == 'function') {
            $('#signature').summernote({
              height: 200,                 // set editor height

              minHeight: null,             // set minimum height of editor
              maxHeight: null,             // set maximum height of editor

              focus: true,                 // set focus to editable area after initializing summernote
            });
        }

        $('#btnAdsCompany').click(function(){
            document.getElementById('adsCompanyForm').reset();
            $('#addCompany').modal();
        });

        $('#btnTags').click(function(){
            document.getElementById('tagsForm').reset();
            $('#tags').modal();
        });

        $('#saveSettings').click(function(){
            Profile.saveSettings();
        });

        $('#saveAdsCompany').click(function(){
            Profile.saveAdsCompany();
        });

        $('.delete-company').click(function(){
            Profile.deleteAdsCompany(this);
        });

        $('#saveTags').click(function(){
            Profile.saveTags();
        });

        $('#confirm-no').click(function(){
           $('#confirmDeleteTags').modal('hide'); 
        });

        $('#confirmDeleteTags').on('hidden.bs.modal', function () {
            $(".second-confirmation").hide();
        });

        $('#confirm-yes').click(function(){
           $('.second-confirmation').show(); 
        });

        $('.delete-tags').click(function(){
            $('#selTagsList option').show();
            $('#selTagsList option[value="'+$(this).attr('data-uuid')+'"]').hide();
            $('#confirm-delete-tags').attr('data-uuid',$(this).attr('data-uuid'));
        });

        $('#confirm-delete-tags').click(function(){
            Profile.deleteTags(this);
        });

        // for ads company modal - update
        $('#profile .table-striped .update-company').click(function(){
            var data = $(this).closest('tr').attr('data-val').split('|');
            $('#company').val(data[0]);
            $('#phone').val(data[1]);
            $('#domain').val(data[2]);
            $('#uuid').val($(this).closest('tr').attr('data-uuid'));
            $('#addCompany').modal();
        });

        // for article tags modal - update
        $('#profile .table-striped .update-tags').click(function(){
            var data = $(this).closest('tr').attr('data-val').split('|');
            $('#tags-title').val(data[0]);
            $('#tags-desc').val(data[1]);
            $('#uuid').val($(this).closest('tr').attr('data-uuid'));
            $('#tags').modal();
        });

        this.addValidation();

        $('.edit-card-button button').click(function() {
            $('.edit-card-button').addClass('hide');
            $('.edit-card-details').addClass('hide');
            $('.edit-card-form').removeClass('hide');
        });

        $('.btn-card-cancel').click(function(){
            $('.edit-card-button').removeClass('hide');
            $('.edit-card-details').removeClass('hide');
            $('.edit-card-form').addClass('hide');
        });

        $('.edit-billing-button button').click(function() {
            $('.edit-billing-button').addClass('hide');
            $('.edit-billing-details').addClass('hide');
            $('.edit-billing-form').removeClass('hide');
        });

        $('.btn-billing-cancel').click(function(){
            $('.edit-billing-button').removeClass('hide');
            $('.edit-billing-details').removeClass('hide');
            $('.edit-billing-form').addClass('hide');
        });
        
        $('.cancel-subscription').click(function(){
            Profile.cancelSubscription();
        });
        
        $('.reactivate-subscription').click(function() {
            Profile.reactivateSubscription(); 
        });

    },
    saveSettings: function() {            
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/settings/account/save',
            success: function (data) {
                if (data.success) {
                    All.progress(50, $('#progressBar') );
                    location.reload();
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            error: function() {
                alert("An error occured. Please refresh the page and try again.");
            },
            data : {
                company : $('#company').val(),
                phone   : $('#phone').val(),
                domain  : $('#domain').val()
            },
        });
    },
    saveAdsCompany: function() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/settings/ads-company/save',
            success: function (data) {
                if (data.success) {
                    All.progress(50, $('#progressBar') );
                    location.reload();
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            error: function() {
                alert("An error occured. Please refresh the page and try again.");
            },
            data : {
                company : $('#company').val(),
                phone   : $('#phone').val(),
                domain  : $('#domain').val(),
                uuid    : $('#uuid').val()
            },
        });
    },
    deleteAdsCompany: function(self) {
        var uuid = $(self).attr('data-uuid');
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/settings/ads-company/delete',
            success: function (data) {
                if (data.success) {
                    All.progress(50, $('#progressBar') );
                    alert("Ads Company Successfully deleted.");
                    location.reload();
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            error: function() {
                alert("An error occured. Please refresh the page and try again.");
            },
            data : {
                uuid : uuid
            },
        });
    },
    saveTags: function() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/settings/tags/save',
            success: function (data) {
                if (data.success) {
                    All.progress(50, $('#progressBar') );
                    location.reload();
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            error: function() {
                alert("An error occured. Please refresh the page and try again.");
            },
            data : $('#tagsForm').serialize(),
        });
    },
    deleteTags: function(self) {
        var uuid = $(self).attr('data-uuid');
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/settings/tags/delete',
            success: function (data) {
                if (data.success) {
                    All.progress(50, $('#progressBar') );
                    alert("Content Tags successfully deleted.");
                    location.reload();
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            error: function() {
                alert("An error occured. Please refresh the page and try again.");
            },
            data : {
                uuid : uuid,
                tagsSelected : $('#selTagsList').val()
            },
        });
    },
    addValidation: function() {

        if ( $('#creditCard').length != 0 ) {
             $('#creditCard').validate({
                rules: {
                    number : {
                        required : true,
                        minlength : 16
                    },
                    cvc  : {
                        required: true,
                        minlength : 3
                    },
                    exp_month: {
                        required : true,
                        maxlength : 2,
                        min: 1,
                        max: 12
                    },
                    exp_year: {
                        required  : true,
                        minlength : 4
                    },
                },
                messages: {
                    number: {
                        required: "Please provide card number.",
                        minlength: "Please input valid card number."
                    },
                    cvc: {
                        required: "Please provide your cvc",
                        minlength: "Please input valid cvc."
                    },
                    exp_month: {
                        required: "Please provide month.",
                        minlength: "Please input valid month.",
                        min: "Please input valid cvc.",
                        max: "Please input valid cvc."
                    },
                    exp_year: {
                        required: "Please provide year.",
                        minlength: "Please input valid year."
                    },
                },
                errorElement: 'label'
            });

            $('.btn-submit-card').click(function(){

                if ($('#creditCard').valid()) {

                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: '/publisher/settings/payment/addUpdateCards',
                        success: function (data) {
                            if (data.success) {
                                alert("Credit Card successfully added.");
                                location.reload();
                            } else {
                                alert(data.message);
                            }
                        },
                        error: function() {
                            alert("An error occured. Please refresh the page and try again.");
                        },
                        data : $('#creditCard').serialize()
                    });
                }
            });

            $("input[name='number'], input[name='cvc'], input[name='exp_month'], input[name='exp_year']").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                     // Allow: Ctrl+C
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                     // Allow: Ctrl+X
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

        }

        if ( $('#billingForm').length != 0 ) {
            $('#billingForm').validate({
                rules: {
                    shipping_name: {
                        required : true,
                        minlength : 3
                    },
                    email: {
                        required: true,
                        minlength : 3,
                        email: true
                    },
                    company: {
                        required : true
                    },
                    address1: {
                        required  : true,
                        minlength : 3
                    },
                    address2: {
                        required  : false,
                        minlength : 3
                    },
                    postal_code: {
                        required  : true,
                        minlength : 4
                    },
                    phone1: {
                        required: true
                    },
                    phone2: {
                        required: false
                    },
                    city: {
                        required: true
                    },
                    country: {
                        required: true
                    },
                    state: {
                        required: true,
                        minlength:3
                    }
                },
                messages: {
                    shipping_name: {
                        required: "Please provide your name.",
                        minlength: "Please input valid name."
                    },
                    email: {
                        required: "Please provide email.",
                        minlength: "Please input valid email.",
                        email: "Please input valid email."
                    },
                    company: {
                        required: "Please provide company."
                    },
                    address1: {
                        required: "Please provide address.",
                        minlength: "Please input valid address."
                    },
                    address2: {
                        minlength: "Please input valid address."
                    },
                    postal_code: {
                        required: "Please provide postal code.",
                        minlength: "Please input valid postal code."
                    },
                    phone1: {
                        required: "Please provide phone."
                    },
                    city: {
                        required: "Please provide city.",
                        minlength: "Please input valid city."
                    },
                    state: {
                        required: "Please provide state.",
                        minlength: "Please input valid state."
                    },
                    country: {
                        required: "Please provide country."
                    }
                },
                errorElement: 'label'
            });

            $("input[name='postal_code']").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                     // Allow: Ctrl+C
                    (e.keyCode == 67 && e.ctrlKey === true) ||
                     // Allow: Ctrl+X
                    (e.keyCode == 88 && e.ctrlKey === true) ||
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $('.btn-submit-billing').click(function(){

                if ($('#billingForm').valid()) {

                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: '/publisher/settings/payment/addUpdateBilling',
                        success: function (data) {
                            if (data.success) {
                                alert("Billing Details successfully added.");
                                location.reload();
                            } else {
                                alert(data.message);
                            }
                        },
                        error: function() {
                            alert("An error occured. Please refresh the page and try again.");
                        },
                        data : $('#billingForm').serialize()
                    });
                }
            });
        }
    },
    cancelSubscription: function() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/settings/payment/cancelSubscription',
            success: function (data) {
                if (data.success) {
                    alert("Subscription successfully cancel.");
                    location.reload();
                } else {
                    alert(data.message);
                }
            },
            error: function() {
                alert("An error occured. Please refresh the page and try again.");
            },
        });
    },
    reactivateSubscription: function() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/settings/payment/reactivateSubscription',
            success: function (data) {
                if (data.success) {
                    alert("Subscription successfully reactivate.");
                    location.reload();
                } else {
                    alert(data.message);
                }
            },
            error: function() {
                alert("An error occured. Please refresh the page and try again.");
            },
        });
    }
}