/* 
 * Dashboard
 */
var gsSwiper;
var Dashboard = Dashboard || {};
    Dashboard = {
        currentDate      : '',
        currentLogsCount : 0,
	init: function(){
		
		$('.ti-content-detail').hover(function(){
			
			$('.ti-content-detail-info', this).animate({ top: '-100px' }, { duration: 'slow', easing: 'easeOutBack' });
			$('.ti-content-detail-actions', this).animate({ top: '10px' }, { duration: 'slow', easing: 'easeOutBack' });

		},
		function(){
			
			$('.ti-content-detail-info', this).animate({ top: '15px' }, { duration: 'slow', easing: 'easeOutBack' });
			$('.ti-content-detail-actions', this).animate({ top: '200px' }, { duration: 'slow', easing: 'easeOutBack' });
			
		});
		
		var dataArticle = [
		    {
		        value: typeof totalCountArticle.active != 'undefined' ? (totalCountArticle.active/totalArticle)*100 : 0,
		        color:"#7FDB7F",
		        highlight: "green",
		        label: "Converted"
		    },
		    {
		        value: typeof totalCountArticle.inprogress != 'undefined' ? (totalCountArticle.inprogress/totalArticle)*100 : 0,
		        color: "#00c7f7",
		        highlight: "#428bca",
		        label: "In Progress"
		    },
		    {
		        value: typeof totalCountArticle.pending != 'undefined' ? (totalCountArticle.pending/totalArticle)*100 : 0,
		        color: "#df4a32",
		        highlight: "red",
		        label: "Pending"
		    }
		   ],
                   dataAds = [
		    {
		        value: typeof totalCountAds.active != 'undefined' ? (totalCountAds.active/totalAds)*100 : 0,
		        color:"#7FDB7F",
		        highlight: "green",
		        label: "Converted"
		    },
		    {
		        value: typeof totalCountAds.inprogress != 'undefined' ? (totalCountAds.inprogress/totalAds)*100 : 0,
		        color: "#00c7f7",
		        highlight: "#428bca",
		        label: "In Progress"
		    }
		];

		// Get context with jQuery - using jQuery's .get() method.
                if (typeof $("#opp_article_heat_chart").get(0) != 'undefined') {
                
                    var ctx = $("#opp_article_heat_chart").get(0).getContext("2d");

                    var myDoughnutChart = new Chart(ctx).Doughnut(dataArticle, {
                        //Boolean - Whether we should show a stroke on each segment
                        segmentShowStroke : true,

                        //String - The colour of each segment stroke
                        segmentStrokeColor : "#fff",

                        //Number - The width of each segment stroke
                        segmentStrokeWidth : 2,

                        //Number - The percentage of the chart that we cut out of the middle
                        percentageInnerCutout : 50, // This is 0 for Pie charts

                        //Number - Amount of animation steps
                        animationSteps : 100,

                        //String - Animation easing effect
                        animationEasing : "easeOutBounce",

                        //Boolean - Whether we animate the rotation of the Doughnut
                        animateRotate : true,

                        //Boolean - Whether we animate scaling the Doughnut from the centre
                        animateScale : false,

                        //String - A legend template
                        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		
                    });
		
                }
                
                if (typeof $("#opp_ads_heat_chart").get(0) != 'undefined') {
                
                    var ctx = $("#opp_ads_heat_chart").get(0).getContext("2d");

                    var myDoughnutChart = new Chart(ctx).Doughnut(dataAds, {
                        //Boolean - Whether we should show a stroke on each segment
                        segmentShowStroke : true,

                        //String - The colour of each segment stroke
                        segmentStrokeColor : "#fff",

                        //Number - The width of each segment stroke
                        segmentStrokeWidth : 2,

                        //Number - The percentage of the chart that we cut out of the middle
                        percentageInnerCutout : 50, // This is 0 for Pie charts

                        //Number - Amount of animation steps
                        animationSteps : 100,

                        //String - Animation easing effect
                        animationEasing : "easeOutBounce",

                        //Boolean - Whether we animate the rotation of the Doughnut
                        animateRotate : true,

                        //Boolean - Whether we animate scaling the Doughnut from the centre
                        animateScale : false,

                        //String - A legend template
                        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
		
                    });
		
                }
                
                monthLabels     = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
                arrDataMonth    = [];
                adsDataMonth    = [];
                dataMonthLabels = [];
                var curMonth   = currentMonth;
                for(month = 0; month < 12 ; month++) {
                    arrDataMonth.push(0);
                    adsDataMonth.push(0);
                    if (typeof monthLabels[curMonth] != 'undefined') {
                        dataMonthLabels.push(monthLabels[curMonth]);
                        curMonth++;
                    } else {
                        curMonth = 0;
                        dataMonthLabels.push(monthLabels[curMonth]);
                        curMonth++;
                    }
                }
                $.each(articleNumPerYear, function(k,v) {
                    var mSelected = (parseInt(v.monthSelected) + parseInt(currentMonth) + 1);
                    if (mSelected > 11) {
                        mSelected = 12 - mSelected;
                    }
                    arrDataMonth[mSelected] = v.articleCount;
                });
                $.each(adsNumPerYear, function(k,v) {
                    var mSelected = (parseInt(v.monthSelected) + parseInt(currentMonth) + 1);
                    if (mSelected > 11) {
                        mSelected = 12 - mSelected;
                    }
                    adsDataMonth[mSelected] = v.adsCount;
                });
                
		var artData = {
		    labels: dataMonthLabels,
		    datasets: [
		        {
		            label: "My Second dataset",
		            fillColor: "rgba(151,187,205,0.2)",
		            strokeColor: "rgba(151,187,205,1)",
		            pointColor: "rgba(151,187,205,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(151,187,205,1)",
		            data: arrDataMonth
		        }
		    ]
		};
                
                var adsData = {
		    labels: dataMonthLabels,
		    datasets: [
		        {
		            label: "My Second dataset",
		            fillColor: "rgba(151,187,205,0.2)",
		            strokeColor: "rgba(151,187,205,1)",
		            pointColor: "rgba(151,187,205,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(151,187,205,1)",
		            data: adsDataMonth
		        }
		    ]
		};
		
                if (typeof $("#avg_article_score_chart").get(0) != 'undefined') {
                    
                    var ctx2 = $("#avg_article_score_chart").get(0).getContext("2d");

                    var myLineChart = new Chart(ctx2).Line(artData, {

		    ///Boolean - Whether grid lines are shown across the chart
		    scaleShowGridLines : true,
		
		    //String - Colour of the grid lines
		    scaleGridLineColor : "rgba(0,0,0,.05)",
		
		    //Number - Width of the grid lines
		    scaleGridLineWidth : 1,
		
		    //Boolean - Whether the line is curved between points
		    bezierCurve : true,
		
		    //Number - Tension of the bezier curve between points
		    bezierCurveTension : 0.4,
		
		    //Boolean - Whether to show a dot for each point
		    pointDot : true,
		
		    //Number - Radius of each point dot in pixels
		    pointDotRadius : 4,
		
		    //Number - Pixel width of point dot stroke
		    pointDotStrokeWidth : 1,
		
		    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
		    pointHitDetectionRadius : 20,
		
		    //Boolean - Whether to show a stroke for datasets
		    datasetStroke : true,
		
		    //Number - Pixel width of dataset stroke
		    datasetStrokeWidth : 2,
		
		    //Boolean - Whether to fill the dataset with a colour
		    datasetFill : true,
		
		    //String - A legend template
		    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
		
		});
                
                }
                
                if (typeof $("#avg_ads_score_chart").get(0) != 'undefined') {
                    
                    var ctx2 = $("#avg_ads_score_chart").get(0).getContext("2d");

                    var myLineChart = new Chart(ctx2).Line(adsData, {

		    ///Boolean - Whether grid lines are shown across the chart
		    scaleShowGridLines : true,
		
		    //String - Colour of the grid lines
		    scaleGridLineColor : "rgba(0,0,0,.05)",
		
		    //Number - Width of the grid lines
		    scaleGridLineWidth : 1,
		
		    //Boolean - Whether the line is curved between points
		    bezierCurve : true,
		
		    //Number - Tension of the bezier curve between points
		    bezierCurveTension : 0.4,
		
		    //Boolean - Whether to show a dot for each point
		    pointDot : true,
		
		    //Number - Radius of each point dot in pixels
		    pointDotRadius : 4,
		
		    //Number - Pixel width of point dot stroke
		    pointDotStrokeWidth : 1,
		
		    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
		    pointHitDetectionRadius : 20,
		
		    //Boolean - Whether to show a stroke for datasets
		    datasetStroke : true,
		
		    //Number - Pixel width of dataset stroke
		    datasetStrokeWidth : 2,
		
		    //Boolean - Whether to fill the dataset with a colour
		    datasetFill : true,
		
		    //String - A legend template
		    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
		
		});
            }
            
            if (totalArticle <= 0) {
    
                $('#getting-started').dialog({
                   width: '1000px',
                   height: 450,
                   resizable: false
                });

                $('.close-gs-button').click(function(){
                    $('#getting-started').hide();
                    $('.ui-dialog').hide();
                });

                gsSwiper = new Swiper('#getting-started-container', {});
            }
            
            this.displayPublisherLogs(publisherLogs);
	},
        displayPublisherLogs: function(logsData) {
            counter = 0;
            $.each(logsData, function(k, v) {
                var template  = $((counter % 2) ? '#logs-article-ads-tpl-alt' : '#logs-article-ads-tpl').html().replace(/{alt}/g, counter % 2 ? 'alt' : '');
                var logStatus = 'Converted'
                    hide      = ''
                    artHide   = ''
                    dateTime  = $.timeago(v.created_at)
                    dateText  = '';
                if (v.status == 'active') {
                    color       = 'green';
                } else if (v.status == 'inprogress') {
                    color = 'blue';
                    hide  = 'hide';
                    if (v.type == 'Ad') {
                        logStatus = 'Created';
                    }
                } else {
                    color     = 'red';
                    logStatus = 'Detected';
                    artHide   = 'hide';
                }
                
                if ( dateTime.indexOf('day') < 0) {
                    dateText = 'Today'
                } else if (dateTime.indexOf('a day') >= 0){
                    dateText = 'Yesterday'
                } else {
                    dateText = dateTime;
                }
                if (Dashboard.currentDate == '' || Dashboard.currentDate != dateText) {
                    var dateTemplate = $('#logs-date').html().replace(/{date-selected}/g, dateText);
                    $('#publisher-logs').append(dateTemplate);
                    Dashboard.currentDate = dateText;
                }
                
                templateContent = $(template).html().replace(/{uuid}/g, v.uuid)
                                                    .replace(/{timeago}/g, $.timeago(v.created_at))
                                                    .replace(/{title}/g, v.title)
                                                    .replace(/{logType}/g, v.type)
                                                    .replace(/{url}/g, v.url)
                                                    .replace(/{logStatus}/g, logStatus)
                                                    .replace(/{icon}/g, v.type == 'Ad' ? 'fa-volume-up' : 'fa-newspaper-o')
                                                    .replace(/{hide}/g, hide)
                                                    .replace(/{artHide}/g, artHide)
                                                    .replace(/{typeUrl}/g, v.type == 'Ad' ? 'ads' : 'contents')
                                                    .replace(/{color}/g, color);

                $('#publisher-logs').append($(template).html(templateContent));
                Dashboard.addEffects();
                counter++;
            });
            Dashboard.currentLogsCount += counter;
        },
        getMoreLogs: function() {
            Dashboard.fetchLogs =  $.ajax({
                url: '/publisher/getMoreLogs',
                dataType: 'json',
                type: 'POST',
                beforeSend : function()    {           
                    if(typeof Dashboard.fetchLogs != 'undefined' && Dashboard.fetchLogs != null) {
                        Dashboard.fetchLogs.abort();
                    }
                },
                success: function(data){
                    if (data.success === true) {
                        All.progress(100, $('#progressBar'));
                        Dashboard.displayPublisherLogs(data.logs);
                    }
                },
                data: { publisherLogs : Dashboard.currentLogsCount}
            });
        },
        addEffects: function() {
            $('.ti-content-detail').hover(function(){
                $('.ti-content-detail-info', this).animate({ top: '-100px' }, { duration: 'slow', easing: 'easeOutBack' });
                $('.ti-content-detail-actions', this).animate({ top: '10px' }, { duration: 'slow', easing: 'easeOutBack' });
            },
            function(){
                $('.ti-content-detail-info', this).animate({ top: '15px' }, { duration: 'slow', easing: 'easeOutBack' });
                $('.ti-content-detail-actions', this).animate({ top: '200px' }, { duration: 'slow', easing: 'easeOutBack' });
            });
        }
}