/* 
 * ALL
 */
var All = All || {};
All = {
    sidebar_width: 0,
    init: function () {
        if ($(window).width() >= 824) {
            $('#global-search #search-art').focus(function () {
                $(this).animate({width: '300px'}, {duration: 'slow', easing: 'easeOutBack'});
            });

            $('#global-search #search-art').blur(function () {
                $(this).animate({width: '250px'}, {duration: 'slow', easing: 'easeOutBack'});
            });
        } else {
            $('#global-search #search-art').focus(function () {
                $(this).animate({width: '90%'}, {duration: 'slow', easing: 'easeOutBack'});
            });

            $('#global-search #search-art').blur(function () {
                $(this).animate({width: '100%'}, {duration: 'slow', easing: 'easeOutBack'});
            });
        }
        
        $('#global-search #search-art').on('keypress', function(e) {
            
            All.search = $.ajax({
                url: '/publisher/search',
                dataType: 'json',
                type: 'POST',
                beforeSend: function () {
                    if (typeof All.search != 'undefined' && All.search != null) {
                        All.search.abort();
                    }
                },
                success: function (result) {
                    $('.result-display').html('');
                    $.each(result, function(k,v) {
                        
                        templateContent = $('#all-search-tpl').html().replace(/{type}/g, v.type)
                            .replace(/{id}/g, v.id)
                            .replace(/{name}/g, v.name);
                        
                        $('.result-display').append(templateContent);
                    });
                },
                data: { 'search-art' : $(this).val() }
            });
            
            if (e.keyCode == 13) {
                return false;
            }
        });
        
        this.notification();
    },
    progress: function (percent, $element) {
        $element.find('div').width(1);
        $element.show();
        var progressBarWidth = percent * $element.width() / 100;
        $element.find('div').animate({width: progressBarWidth}, 500, function () {
            $element.fadeOut(8000);
        }).html("<span>" + percent + "% Saved.</span>");
    },
    notification: function() {
        if ($('.card-notifyer').length != 0) {
            $('.sidebar, .main-body').addClass('adjust-height');
            
            $('.card-notifyer .close').click(function(){
                $('.sidebar, .main-body').removeClass('adjust-height');
                $('.card-notifyer').hide();
            });
        }

        if ($('#activateBanner').length != 0) {
            $('.sidebar, .main-body').addClass('adjust-height2');

            $('#activateBanner .close').click(function(){
                $('.sidebar, .main-body').removeClass('adjust-height2');
                $('#activateBanner').hide();
            });
        }
    }
}