/* 
 * ADS
 */
var Ads = Ads || {};
Ads = {
    currentAds: 0,
    archivedPage: 0,
    filterType: 'all',
    init: function () {
        Ads.effects();

        $('#activity-stats [data-type]').click(function (e) {
            e.preventDefault();

            $('#activity-stats [data-type]').removeClass('active');
            $(this).addClass('active');

            $('#ads > div').remove();
            $('.ads-actions').show();

            Ads.filterType = $(this).attr('data-type');
            Ads.currentAds = 0;
            Ads.fetchAdsData(true);
            $('#activity-clear-filter').fadeIn('fast');

        });

        $('#activity-clear-filter').click(function (e) {

            e.preventDefault();

            $('#activity-stats [data-type]').removeClass('active');
            $('#ads > div').remove();
            $('.ads-actions').show();

            Ads.filterType = 'all';
            Ads.currentAds = 0;
            Ads.fetchAdsData(true);
            $('#activity-clear-filter').fadeOut('fast');

        });

        $('#saveNewAds').click(function (e) {
            e.preventDefault();
            Ads.createNew();
        });

        $('#ActivityTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $('#addNewAd').on('show.bs.modal', function (e) {
            $('#new-ad-modal-step1').animate({top: '0px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#new-ad-modal-step2').animate({top: '500px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#new-ad-modal-step3').animate({top: '-1000px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#new-ad-modal').animate({height: '250px'}, {duration: 'slow', easing: 'easeOutBack'});
        });

        $('#action-upload').click(function (e) {
            $('#errorMessages, #errorMessages2').hide();
            $('#errorMessages li,#errorMessages2 li').remove();
            $('#new-ad-modal-step1').animate({top: '-490px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#new-ad-modal-step2').animate({top: '0px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#new-ad-modal').animate({height: '500px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#files').html('');
            $('#progress .progress-bar').css('width', '0%');
            $('#createads input[name="adsName"]').val('');
        });

        $('#create-ad').click(function (e) {
            $('#errorMessages, #errorMessages2').hide();
            $('#errorMessages li,#errorMessages2 li').remove();
            $('#new-ad-modal-step1').animate({top: '560px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#new-ad-modal-step2').animate({top: '560px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#new-ad-modal-step3').animate({top: '-20px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('#new-ad-modal').animate({height: '550px'}, {duration: 'slow', easing: 'easeOutBack'});
        });

        if (typeof max_archived_list != "undefined") {
            this.archivedPage = 1;
            Ads.displayArchiveList(true, ads);
        } else {
            this.archivedPage = 0;
            Ads.displayListing(ads);
        }

        $('#convertVoiceAds').click(function (e) {
            e.preventDefault();
            Ads.convertVoiceAds();
        });

        $('.ads-actions-btn').click(function () {
            Ads.fetchAdsData(false);
        });

        $('.ads-archived-btn')

        $('#btnarchivedAds').click(function () {
            Ads.archived(this);
        });

        $('#archivedAds').on('hidden.bs.modal', function () {
            location.href = '/publisher/ads';
        });

        Ads.initializeUploader();

        // for selecting the selected status onload
        if ($.inArray(selectedStatus, ['active', 'inprogress']) >= 0) {
            $('div[data-type="' + selectedStatus + '"]').click();
        }

        $('.create-new-ads').click(function () {
            if (haveCards) {
                if (isAddAllowed != 0) {
                    $('#addNewAd').modal();
                } else {
                    $('#packageNotification').modal();
                }
            } else {
                $('#cardsNotification').modal();
            }
        });

        $('#packageNotification .notif-yes').click(function () {
            $('#packageNotification').modal('hide');
            $('#addNewAd').modal();
        });

        $('#packageNotification .notif-no').click(function () {
            $('#packageNotification').modal('hide');
        });
        
        $('#cardsNotification .notif-close').click(function () {
            $('#cardsNotification').modal('hide');
        });
    },
    fetchAdsData: function (reset) {

        var dataVal = {};
        if (reset) {
            dataVal = {offset: 0,
                status: this.filterType,
                archivedFlag: this.archivedPage,
                companyId: companyId};
        } else {
            dataVal = {offset: this.currentAds,
                status: this.filterType,
                archivedFlag: this.archivedPage,
                companyId: companyId};
        }

        Ads.fetchAds = $.ajax({
            url: '/publisher/ads/fetchAdsList',
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                if (typeof Ads.fetchAds != 'undefined' && Ads.fetchAds != null) {
                    Ads.fetchAds.abort();
                }
            },
            success: function (data) {
                if (data.success === true) {
                    All.progress(100, $('#progressBar'));

                    if (Ads.archivedPage == true) {
                        Ads.displayArchiveList(false, data.ads);
                    } else {
                        Ads.displayListing(data.ads);
                    }
                }
            },
            data: dataVal
        });
    },
    initializeUploader: function () {
        $(function () {
            'use strict';
            $('#audioFile').fileupload({
                url: '/publisher/ads/fileHandler',
                dataType: 'json',
                change: function (e, data) {
                    $('#progress .progress-bar').css('width', '0%');
                },
                done: function (e, data) {
                    $('#files').html(data.result.data);
                    $('input[name="fileName"]').val(data.result.data);
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                            'width',
                            progress + '%'
                            );
                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('#btn-upload').click(function (e) {
                e.preventDefault();
                $('#audioFile').click();
            });
        });
    },
    createNew: function () {

        $('#errorMessages').hide();
        $('#errorMessages li').remove();

        if ($('#files').html().length == 0) {
            var icons = '<i class="fa fa-exclamation-triangle"></i>';
            if ($('#adsName').val().length == 0) {
                $('#errorMessages').append("<li>" + icons + "Please entry Ads Name</li>");
            }
            $('#errorMessages').append("<li>" + icons + "File must be uploaded first</li>");
            $('#errorMessages').show();
            return;
        }

        All.progress(50, $('#progressBar'));
        var adsCompanyId = $('#adsCompanyId').val();
        var adsName = $('#adsName').val();
        var adsFileName = $('#files').html();
        var formData = new FormData();
        formData.append('adsCompanyId', adsCompanyId);
        formData.append('adsName', adsName);
        formData.append('adsFileName', adsFileName);
        $.ajax({
            url: '/publisher/ads/createNew',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            type: 'POST',
            success: function (data) {
                if (data.success === true) {
                    All.progress(100, $('#progressBar'));
                    $('#uploadAudioFileMessage').append("<li>Upload Audio File Success!</li>");
                    $('#addNewAd').modal('hide');
                    location.reload();
                } else {
                    var icons = '<i class="fa fa-exclamation-triangle"></i>';
                    for (i in data.message) {
                        $('#errorMessages1').append("<li>" + icons + data.message[i] + "</li>");
                    }
                    $('#errorMessages1').show();
                }
            }
        });
    },
    convertVoiceAds: function () {
        $('#errorMessages2').hide();
        $('#errorMessages2 li').remove();
        $.ajax({
            url: '/publisher/ads/convertVoiceAds',
            dataType: 'json',
            data: $('#createadsconversion').serialize(),
            type: 'POST',
            success: function (data) {

                if (data.success === true) {
                    All.progress(100, $('#progressBar'));
                    $('#uploadAudioFileMessage').append("<li>Convert Ads Success!</li>");
                    $('#addNewAd').modal('hide');
                    location.reload();
                } else {
                    var icons = '<i class="fa fa-exclamation-triangle"></i>';
                    for (i in data.message) {
                        $('#errorMessages2').append("<li>" + icons + data.message[i] + "</li>");
                    }
                    $('#errorMessages2').show();
                }
            }
        });
    },
    manage: function () {

        // main edit fields
        $('#manage-ad').on('click', '.size-h1', function (e) {

            if (!$(this).hasClass('editing')) {

                var content = $(this).text();
                if ($(this).hasClass('default')) {
                    $(this).text('')
                }
                $(this).attr('data-default', content).addClass('editing').focus();

            }

        }).on('blur', '.size-h1', function () {

            var contents = $(this).html();

            if (contents != '' && contents != $(this).attr('data-default')) {

                All.progress(50, $('#progressBar'));

                /*
                 $.ajax({
                 dataType: "xml",
                 type: "POST",
                 url: "/fieldsave",
                 success: Ads.fieldsaved,
                 data: {
                 content: contents,
                 field: $(this).attr('data-field'),
                 id: $('#action_id').val()
                 }
                 });
                 */

                $(this).removeClass('default');

            } else {

                $(this).text($(this).attr('data-default'));

            }

            $(this).removeClass('editing');

        });

        $('.assignment-table tr input[type="checkbox"]').click(function () {
            Ads.assignedAdsArticle(this);
        });

        $('.archived-assignment-table tr input[type="checkbox"]').click(function () {
            Ads.assignedAdsArticle(this);
        });

        $('#reconvert').click(function () {
            Ads.reconvert(this);
        });

        $('#conversion').click(function () {
            document.getElementById('frm-convert-ads').reset();
            var uuid = $(this).attr('data-uuid');
            $('#uuid').val(uuid);
        });

        // convert audio article
        $("#convert-btn").click(function () {
            Ads.beginConversion();
        });

        // upload audio file
        $("#uploadAudioButton").click(function () {
            Ads.uploadAdsFile();
        });

        $('#btnManageUpload').click(function () {
            $('#errorMessages').hide();
            $('#errorMessages li').remove();
            $('#files').html('');
            $('#progress .progress-bar').css('width', '0%');
            $('#uploadAdsAudio input[name="fileName"]').val('');
            $('#uploadAdsAudio textarea[name="fileDesc"]').val('');
        });

        //jquery library for searching text
        $.extend($.expr[":"], {
            "containsIN": function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $('#search-prepostplay').on('keyup', function (e) {
            $('.assignment-table tbody tr').show();
            if ($.trim($(this).val()).length != 0) {
                $('.assignment-table tbody tr:not(:containsIN("' + $(this).val() + '"))').hide();
            } else {
                $('.assignment-table tbody tr').show();
            }
        });

        $('#archivedAds').on('hidden.bs.modal', function () {
            if (Ads.activateSuccessful) {
                location.href = '/publisher/ads';
            }
        });

        // for initializing uploader
        this.initializeUploader();

    },
    activity: function () {

        Ads.displayGraph(adsActivityDetails);

        var curMonth = currentMonth;
        curMonth++;
        var disableMonth = [];
        while (curMonth <= 12) {
            disableMonth.push(curMonth);
            curMonth++;
        }
        // for changing Month
        var options = {
            selectedYear: currentYear,
            selectedMonth: 2015,
            startYear: 2015,
            finalYear: currentYear,
            openOnFocus: true,
            pattern: 'mm/yyyy'
        };
        $('#selectedMonth').monthpicker(options).bind('monthpicker-click-month', function () {
            var arrDate = $(this).val().split('/');
            $.ajax({
                dataType: "json",
                type: "POST",
                url: '/publisher/ads/get-track-details/' + adsUuid + '/' + arrDate[1] + '/' + arrDate[0],
                success: function (data) {
                    Ads.displayGraph(data.adsActivityDetails);
                },
                error: function () {
                    alert("An error occured. Please refresh the page and try again.");
                }
            });
        });
        $('#selectedMonth').on('click', function () {
            $('#selectedMonth').monthpicker('disableMonths', []);
            var arrDate = $(this).val().split('/');
            if (arrDate[1] == currentYear) {
                $(this).monthpicker('disableMonths', disableMonth);
            }
        });
    },
    displayGraph: function (adsActivityData) {

        var dataLabels = [];
        var dataValue = [];

        for (days = 0; days < maxDays; days++) {
            dataValue.push(0);
            dataLabels.push(String(days + 1));
        }
        $.each(adsActivityData, function (k, v) {
            dataValue[v.dateSelected - 1] = v.adsCount;
        });

        var data = {
            labels: dataLabels,
            datasets: [
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: dataValue
                }
            ]
        };

        var ctx2 = $("#daily-ad-plays").get(0).getContext("2d");
        if (typeof Ads.myLineChart != 'undefined') {
            Ads.myLineChart.destroy();
        }
        Ads.myLineChart = new Chart(ctx2).Line(data, {
            responsive: true,
            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.4,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a colour
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

        });

    },
    effects: function () {

        $('#ads section .panel-right').hover(function () {

            $('.panel-right-summary', this).animate({top: '-100px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('.panel-right-actions', this).animate({top: '20px'}, {duration: 'slow', easing: 'easeOutBack'});

        },
                function () {

                    $('.panel-right-summary', this).animate({top: '20px'}, {duration: 'slow', easing: 'easeOutBack'});
                    $('.panel-right-actions', this).animate({top: '200px'}, {duration: 'slow', easing: 'easeOutBack'});

                });

    },
    displayListing: function (ads) {

        var counter = 0;

        $.each(ads, function (k, v) {

            if (typeof v != "undefined") {
                var color = '';

                if (v.status == 'active') {
                    color = 'green';
                } else if (v.status == 'inprogress') {
                    color = 'blue';
                } else {
                    color = 'red';
                }
                //replace all ads data in template
                templateContent = $('#ads-design').html().replace(/{uuid}/g, v.uuid)
                        .replace(/{ads-title}/g, v.title)
                        .replace(/{art-count}/g, v.articleCount)
                        .replace(/{status}/g, v.status)
                        .replace(/{hide}/g, v.status != 'active' ? 'hide' : '')
                        .replace(/{ads-desc}/g, ((typeof v.description != "undefined" && v.description != null) ? v.description : ""))
                        .replace(/{color}/g, color);
                $('#ads').append(templateContent);
                counter++;
            } else {
                $('.ads-actions').hide();
            }
        });
        Ads.currentAds += counter;
        this.effects();
    },
    displayArchiveList: function (onload, ads) {
        var counter = 0;

        if (typeof ads != 'undefined') {
            $.each(ads, function (k, v) {

                if (typeof v != "undefined") {
                    var color = '';

                    if (v.status == 'active') {
                        color = 'green';
                    } else if (v.status == 'inprogress') {
                        color = 'blue';
                    } else {
                        color = 'red';
                    }
                    //replace all ads data in template
                    templateContent = $('#ads-design').html().replace(/{uuid}/g, v.uuid)
                            .replace(/{ads-title}/g, v.title)
                            .replace(/{art-count}/g, v.articleCount)
                            .replace(/{status}/g, v.status)
                            .replace(/{ads-desc}/g, ((typeof v.description != "undefined" && v.description != null) ? v.description : ""))
                            .replace(/{color}/g, color);
                    $('#ads').append(templateContent);
                    counter++;
                } else {
                    $('.ads-actions').hide();
                }
            });
        } else {
            if (onload) {
                $('.ui-timeline').html('No archive ads found.');
            }
        }
        Ads.currentAds += counter;
        this.effects();
    },
    archived: function (_this) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/ads/archived/' + $(_this).attr('data-uuid'),
            success: function (data) {
                if (data.success) {
                    alert('Ads has been successfully archived.');
                    location.href = '/publisher/ads';
                } else {
                    alert("Ads can't be archived. Please refresh the page and try again.");
                }
            },
            error: function () {
                alert("An error occured. Please refresh the page and try again.");
            }
        });
    },
    reconvert: function (_this) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/ads/reconvert/' + $(_this).attr('data-uuid'),
            success: function (data) {
                if (data.success) {
                    alert('Ads has been successfully converted.');
                    location.href = "/publisher/ads";
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            error: function () {
                alert("An error occured. Please refresh the page and try again.");
            }
        });
    },
    beginConversion: function () {
        All.progress(50, $('#progressBar'));
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "/publisher/ads/convert",
            success: function (data) {
                All.progress(100, $('#progressBar'));
                if (data.success) {
                    alert("Ads is successfully convert.");
                    $('#convertAds').modal('hide');
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            data: $('#frm-convert-ads').serialize()
        });
    },
    uploadAdsFile: function () {
        $('#errorMessages').hide();
        $('#errorMessages li').remove();
        All.progress(50, $('#progressBar'));
        $.ajax({
            url: '/publisher/ads/saveUploadedFile',
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                $('#uploadAudioFileMessage').html("");
                if (data.upload === true) {
                    All.progress(100, $('#progressBar'));
                    $('#uploadAudioFileMessage').append("<li>Upload Audio File Success!</li>");
                    Ads.activateSuccessful = true;

                } else {
                    $('#errorMessages').show();
                    for (i in data.message) {
                        $('#errorMessages').append("<li>" + data.message[i] + "</li>");
                    }
                }
            },
            data: $('#uploadAdsFile').serialize()
        });
        
        $('#uploadAdsAudio').on('hidden.bs.modal', function () {
            if (Ads.activateSuccessful) {
                location.reload();
            }
        });
    },
    assignedAdsArticle: function (_this) {

        var dataVal = {'article-uuid': $(_this).attr('data-article-uuid'),
            'ads-uuid': $(_this).attr('data-ads-uuid'),
            'action-type': $(_this).attr('data-action-type'),
            'isChecked': $(_this).prop('checked')};

        $.ajax({
            url: '/publisher/ads/saveAdsArticle',
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                if (data.success === true) {
                    All.progress(100, $('#progressBar'));
                }
            },
            data: dataVal
        });
    },
}