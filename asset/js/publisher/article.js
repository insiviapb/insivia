/* 
 * ARTICLES
 */
var Articles = Articles || {};
Articles = {
    filterType: 'all',
    tagsSelected: [],
    currentArticle: 0,
    archivedPage: 0,
    init: function () {

        this.triggerOnHover();

        $('#activity-stats [data-type]').click(function (e) {
            e.preventDefault();

            $('#activity-stats [data-type]').removeClass('active');
            $(this).addClass('active');

            $('.ui-timeline article').remove();
            $('.acticle-actions').show();
            $('#activity-clear-filter').fadeIn('fast');
            Articles.filterType = $(this).attr('data-type');
            Articles.currentArticle = 0;
            Articles.fetchArticleData(true);

        });

        $('#activity-clear-filter').click(function (e) {
            e.preventDefault();

            $('#activity-stats [data-type]').removeClass('active');
            $('.ui-timeline article').remove();
            $('#activity-clear-filter').fadeOut('fast');
            $('.acticle-actions').show();
            Articles.filterType = 'all';
            Articles.currentArticle = 0;
            Articles.fetchArticleData(false);

        });

        $('#ActivityTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        if (typeof max_archived_list != "undefined") {
            this.archivedPage = 1;
            Articles.displayArchiveList(true, articles);
        } else {
            this.archivedPage = 0;
            Articles.displayListing(articles);
        }

        // for responsiveness of articles
        $(window).resize(function () {
            if ($(window).width() < '768') {
                $('article').removeClass('alt');
            } else {
                if ($('article.alt').length == 0) {
                    $('article.rposition').addClass('alt');
                }
            }
        });

        $('.acticle-actions').click(function () {
            Articles.fetchArticleData(false);
        });

        $('.archived-actions').click(function () {
            Articles.fetchArticleData(false);
        });

        // for selecting the selected status onload
        if ($('#archived-list').length < 0) {
            if ($.inArray(selectedStatus, ['active', 'inprogress', 'pending']) >= 0) {
                $('div[data-type="' + selectedStatus + '"]').click();
            }
        }
    },
    fetchArticleData: function (reset) {

        var dataVal = {};
        if (reset) {
            dataVal = {offset: 0,
                status: this.filterType,
                archivedFlag: this.archivedPage,
                tag: tagged};
        } else {
            dataVal = {offset: this.currentArticle,
                status: this.filterType,
                archivedFlag: this.archivedPage,
                tag: tagged};
        }

        Articles.fetchArticle = $.ajax({
            url: '/publisher/article/fetchArticleList',
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                if (typeof Articles.fetchArticle != 'undefined' && Articles.fetchArticle != null) {
                    Articles.fetchArticle.abort();
                }
            },
            success: function (data) {
                if (data.success === true) {
                    All.progress(100, $('#progressBar'));

                    if (Articles.archivedPage == true) {
                        Articles.displayArchiveList(true, data.article);
                    } else {
                        Articles.displayListing(data.article);
                    }
                }
            },
            data: dataVal
        });
    },
    manage: function () {

        $('a[href="#schedule"]').click(function (e) {
            e.preventDefault()
            $('#schedule-content').slideDown();
        });

        $('#schedule-date').datepicker({});

        $('.assignment-table tr').click(function () {

            $('.assignment-table tr').removeClass('active');
            $(this).addClass('active');
            if ($(this).hasClass('active')) {
                $(this).find('input[type="radio"]').prop('checked', true);
            } else {
                $(this).find('input[type="radio"]').prop('checked', false);
            }

        });

        // main edit fields
        $('#manage-article').on('click', '.size-h1', function (e) {
            if (!$(this).hasClass('editing')) {

                var content = $(this).text();
                if ($(this).hasClass('default')) {
                    $(this).text('')
                }
                $(this).attr('data-default', content).addClass('editing').focus();

            }

        }).on('blur', '.size-h1', function () {
            var contents = $(this).html();
            if (contents != '' && contents != $(this).attr('data-default')) {
                All.progress(50, $('#progressBar'));

                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: "/publisher/article/xml/update",
                    success: function (data) {
                        All.progress(100, $('#progressBar'));
                    },
                    data: {
                        uuid: $(this).attr('data-uuid'),
                        content: contents,
                        field: $(this).attr('data-field'),
                    }
                });

                $(this).removeClass('default');
            } else {
                $(this).text($(this).attr('data-default'));
            }

            $(this).removeClass('editing');
        });

        // main edit fields
        $('#manage-article').on('click', '.text', function (e) {
            if (!$(this).hasClass('editing')) {
                var content = $(this).text();
                if ($(this).hasClass('default')) {
                    $(this).text('')
                }

                $(this).attr('data-default', content).addClass('editing').focus();
            }
        }).on('blur', '.text', function () {
            var contents = $(this).html();
            if (contents != '' && contents != $(this).attr('data-default')) {
                All.progress(50, $('#progressBar'));

                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: "/publisher/article/xml/update",
                    success: function (data) {
                        All.progress(100, $('#progressBar'));
                    },
                    data: {
                        uuid: $(this).attr('data-uuid'),
                        content: contents,
                        field: $(this).attr('data-field'),
                    }
                });

                $(this).removeClass('default');
            } else {
                $(this).text($(this).attr('data-default'));
            }

            $(this).removeClass('editing');
        });

        // upload audio file
        $("#uploadAudioButton").click(function () {
            var currCount = activeArticles + 1;
            if (currCount > maxArticleLimit) {
                $('#packageNotification').modal();
            } else {
                Articles.uploadAudioFile();
            }
        });
        
        $('.btn-activate-article').click(function(){
            if (haveCards) {
                $('#uploadAudio').modal();
            } else {
                $('#cardsNotification').modal();
            }
        });
        
        $('.notif-close').click(function(){
            $('#cardsNotification').modal('hide');
        });
        
        $('#packageNotification .notif-yes').click(function(){
            $('#packageNotification').modal('hide');
            Articles.uploadAudioFile();
        });
        $('#packageNotification .notif-no').click(function(){
            $('#packageNotification').modal('hide');
        });

        $("#uploadAudioImage").click(function () {
            All.progress(50, $('#progressBar'));
            var imageFile = $('.audio-image #files').html();
            if (imageFile.match(/(gif|png|jpg|jpeg)$/) != null) {
                $('#uploadImage #uploadAudioFileMessage').hide();
                $('#uploadImage #uploadAudioFileMessage').html("");
                var formData = new FormData();
                var audioUuid = $('.audio-image #uploadAudioUuid').val();
                formData.append('uuid', audioUuid);
                formData.append('imageFile', imageFile);
                $.ajax({
                    url: '/publisher/article/xml/audioImage',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    type: 'POST',
                    success: function (data) {
                        $('#uploadImage #uploadAudioFileMessage').show();
                        if (data.upload === true) {
                            All.progress(100, $('#progressBar'));
                            $('#uploadImage #uploadAudioFileMessage').append("<li>Upload Audio Image Success!</li>");
                        } else {
                            for (i in data.message) {
                                $('#uploadImage #uploadAudioFileMessage').append("<li>" + data.message[i] + "</li>");
                            }
                        }
                    }
                });
            } else {
                $('#uploadImage #uploadAudioFileMessage').html("");
                $('#uploadImage #uploadAudioFileMessage').html("<span>Upload Audio Image Error</span>");
            }
        });

        $('#conversion').click(function () {
            $('.notes-details a').click();
            document.getElementById('frm-convert-article').reset();
            var uuid = $(this).attr('data-uuid');
            $('#uuid').val(uuid);
        })

        // convert audio article
        $("#convert-btn").click(function () {
            
            $('#termsAgreement').modal('show');
            $('#convertArticle').modal('hide');
        });
        
        $('.terms-agree').click(function(){
            console.log($('#frm-convert-article').serialize());
            All.progress(50, $('#progressBar'));
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/publisher/article/xml/convert",
                success: function (data) {
                    All.progress(100, $('#progressBar'));
                    if (data.convertion) {
                        alert("Contents is successfully convert.");
                        $('#convertArticle').modal('hide');
                        $('#termsAgreement').modal('hide');
                        location.reload();
                    } else {
                        alert("An error occured. Please refresh the page and try again.");
                    }
                },
                data: $('#frm-convert-article').serialize()+'&waitingduration='+$('#sct-duration option:selected').val()
            });
            
        });
        
        $('.terms-back').click(function() {
            $('#termsAgreement').modal('hide');
            $('#convertArticle').modal('show');
        });
        
        // schedule convertion
        $("#scheduleConversion").click(function () {
            All.progress(50, $('#progressBar'));
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/publisher/article/xml/convert",
                success: function (data) {
                    All.progress(100, $('#progressBar'));
                },
                data: {
                    uuid: $(this).attr('data-uuid'),
                }
            });
        });

        //jquery library for searching text
        $.extend($.expr[":"], {
            "containsIN": function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $('#search-preplay, #search-postplay').on('keyup', function (e) {
            var searchId = ($(this).attr('id')).split('-');
            $('.assignment-' + searchId[1] + '-table tr').show();
            if ($.trim($(this).val()).length != 0) {
                $('.assignment-' + searchId[1] + '-table tr:not(:containsIN("' + $(this).val() + '"))').hide();
            } else {
                $('.assignment-' + searchId[1] + '-table tr').show();
            }
        });


        $('input[name="radPre"]').click(function () {
            Articles.addAds(this);
        });

        $('input[name="radPost"]').click(function () {
            Articles.addAds(this);
        });

        $(function () {
            var ms = $('#ms').magicSuggest({
                placeholder: 'Type some tags or select tags',
                maxSelection: null,
                data: articleTags
            });
            ms.setValue(selectedTags);
            $(ms).on('selectionchange', function () {
                var msData = this.getValue();
                var arr = Object.keys(msData).map(function (k) {
                    return msData[k]
                });
                Articles.tagsSelected = arr;
                Articles.addTagging($('[data-uuid]'));
            });
        });

        $('#uploadAudio').on('hide.bs.modal', function (e) {
            $('#progress .progress-bar').css('width', '0%');
            $('.audio-file #files').html('');
            $('.audio-image #files').html('');
            $('#uploadAudioDesc').html('');

            if (Articles.activateSuccess) {
                location.reload();
            }
        });

        $('#uploadImage').on('hide.bs.modal', function (e) {
            $('#progress .progress-bar').css('width', '0%');
            $('.audio-image #files').html('');
            $('.audio-file #files').html('');
            $('#uploadAudioDesc').html('');
        });
        
        $('.vt-details, .profile-details').click(function() {
           $('#vtuuid').val($(this).data('vtuuid'));
           $('.amount-per-minute').html($(this).data('amount'));
           $('.select-voicetalent').addClass('hide');
           $('.add-notes').removeClass('hide');
           $('#convertArticle #article-modal').css('height', '345px');
           $('#convertArticle .modal-dialog').css('width', '445px');
        });
        
        $('.notes-details a').click(function() {
            $('.add-notes').addClass('hide');
            $('.select-voicetalent').removeClass('hide');
            $('#vtuuid').val('');
            $('#convertArticle #article-modal').css('height', '550px');
            $('#convertArticle .modal-dialog').css('width', '1000px');
        });
        
        $(".article-vt-approved").click(function () {
            $('#vtApprovalNotification').modal('show');
            $('.vt-notif-yes').attr('data-uuid', $(this).data('uuid'));
        });
        
        $('.vt-notif-yes').click(function() {
            $('.vt-notif-yes .cancel-loader').removeClass('hide');
            All.progress(50, $('#progressBar'));
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/publisher/article/approve-voicetalent",
                success: function (data) {
                    $('.vt-notif-yes .cancel-loader').addClass('hide');
                    All.progress(100, $('#progressBar'));
                    location.reload();
                },
                data: {
                    notes: $('#vt-approval-notes').val(),
                    uuid : $(this).data('uuid')
                }
            });
        });
        
        $('.vt-notif-no').click(function() {
            $('#vtApprovalNotification').modal('hide');
        });
        
        $(".article-vt-declined").click(function () {
            All.progress(50, $('#progressBar'));
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/publisher/article/decline-voicetalent",
                success: function (data) {
                    All.progress(100, $('#progressBar'));
                    location.reload();
                },
                data: {
                    notes: $('#vt-approval-notes').val(),
                    uuid : $(this).data('uuid')
                }
            });
        });
        
        $('.play-sample').click(function(){
           
           $.ajax({
                dataType: "json",
                type: "POST",
                url: "/publisher/voice-talent/check-samples",
                success: function (data) {
                    if ( data.success ) {
                        $('.list-item-vt-samples[data-vtuuid="'+data.vtuuid+'"]').removeClass('hide')
                        $('.list-item-vt-samples[data-vtuuid="'+data.vtuuid+'"]').html('');
                        $.each(data.samples, function(k,v) {
                            var template = $('#audio-list-item').html().replace(/{path}/g, v.path).replace(/{title}/g, v.title);
                            $('.list-item-vt-samples[data-vtuuid="'+data.vtuuid+'"]').append(template);
                        });
                        var template2 = $('#audio-list-hide').html().replace(/{vtuuid}/g, data.vtuuid);
                        $('.list-item-vt-samples[data-vtuuid="'+data.vtuuid+'"]').append(template2);
                        $('.btn-hide').click(function(){
                            $('.list-item-vt-samples[data-vtuuid="'+$(this).data('vtuuid')+'"]').addClass('hide')
                        });
                    }
                },
                data: {
                    vtuuid: $(this).data('vtuuid')
                }
            });
        });
        
        $('.btn-cancel-conversion').click(function() {
            $(this).find('.cancel-loader').removeClass('hide');
            $.ajax({
                dataType: "json",
                type: "POST",
                url: "/publisher/voice-talent/cancel-conversion",
                success: function (data) {
                    if ( data.success ) {
                        location.reload()
                    } else {
                        alert("An error occured. Please refresh the page and try again.");
                    }
                },
                data: {
                    vaaid: $(this).data('vaaid')
                }
            });
        });
        
        if ( $('#rate-voicetalent').length > 0) {
            $(function() {
                $('#rate-voicetalent').barrating({
                  theme: 'fontawesome-stars',
                  onSelect:function(value, text, event) {
                      $('#rate-reason').removeClass('hide');
                      $('.rate-actions').removeClass('hide');
                  }
                });
                $('#rate-voicetalent').removeClass('hide');
             });
             
             $('#rate-no').click(function() {
                 $('#rate-reason').addClass('hide');
                 $('#rate-reason').val('');
                 $('.rate-actions').addClass('hide');
             });
             $('#rate-yes').click(function() {
                 $('#rate-loader').removeClass('hide');
                 $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: "/publisher/voice-talent/rate-voicetalent",
                    success: function (data) {
                        if ( data.success ) {
                            alert('Voice Talent is successfully rated. thanks');
                            location.reload()
                        } else {
                            alert("An error occured. Please refresh the page and try again.");
                        }
                    },
                    data: $('#rate-vt-service').serialize()
                });
             })
             
        }
        
        this.initializeUploader();
    },
    activity: function () {

        Articles.displayGraph(articleActivityDetails);

        var curMonth = currentMonth;
        curMonth++;
        var disableMonth = [];
        while (curMonth <= 12) {
            disableMonth.push(curMonth);
            curMonth++;
        }
        // for changing Month
        var options = {
            selectedYear: currentYear,
            selectedMonth: 2015,
            startYear: 2015,
            finalYear: currentYear,
            openOnFocus: true,
            pattern: 'mm/yyyy'
        };
        $('#selectedMonth').monthpicker(options).bind('monthpicker-click-month', function () {
            var arrDate = $(this).val().split('/');
            $.ajax({
                dataType: "json",
                type: "POST",
                url: '/publisher/article/get-track-details/' + articleUuid + '/' + arrDate[1] + '/' + arrDate[0],
                success: function (data) {
                    Articles.displayGraph(data.articleActivityDetails);
                },
                error: function () {
                    alert("An error occured. Please refresh the page and try again.");
                }
            });
        });
        $('#selectedMonth').on('click', function () {
            $('#selectedMonth').monthpicker('disableMonths', []);
            var arrDate = $(this).val().split('/');
            if (arrDate[1] == currentYear) {
                $(this).monthpicker('disableMonths', disableMonth);
            }
        });
    },
    displayGraph: function (articleActivityData) {

        var dataLabels = [];
        var dataValue = [];

        for (days = 0; days < maxDays; days++) {
            dataValue.push(0);
            dataLabels.push(String(days + 1));
        }
        $.each(articleActivityData, function (k, v) {
            dataValue[v.dateSelected - 1] = v.articleCount;
        });

        var data = {
            labels: dataLabels,
            datasets: [
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: dataValue
                }
            ]
        };

        var ctx2 = $("#daily-article-plays").get(0).getContext("2d");
        if (typeof Articles.myLineChart != 'undefined') {
            Articles.myLineChart.destroy();
        }
        Articles.myLineChart = new Chart(ctx2).Line(data, {
            responsive: true,
            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.4,
            //Boolean - Whether to show a dot for each point
            pointDot: true,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a colour
            datasetFill: true,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

        });

    },
    initializeUploader: function () {
        $(function () {
            'use strict';
            $('.audio-file #audioFile').fileupload({
                url: '/publisher/article/fileHandler',
                dataType: 'json',
                change: function (e, data) {
                    $('.audio-file #progress .progress-bar').css('width', '0%');
                },
                done: function (e, data) {
                    $('.audio-file #files').html(data.result.data);
                    $('.audio-file input[name="fileName"]').val(data.result.data);
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('.audio-file #progress .progress-bar').css(
                            'width',
                            progress + '%'
                            );
                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('.audio-image #audioImageFile').fileupload({
                url: '/publisher/article/imageHandler',
                dataType: 'json',
                change: function (e, data) {
                    $('.audio-image #progress .progress-bar').css('width', '0%');
                },
                done: function (e, data) {
                    $('.audio-image #files').html(data.result.data);
                    $('.audio-image input[name="fileName"]').val(data.result.data);
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('.audio-image #progress .progress-bar').css(
                            'width',
                            progress + '%'
                            );
                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');

            $('.audio-image #btn-upload').click(function (e) {
                e.preventDefault();
                $('#audioImageFile').click();
            });

            $('.audio-file #btn-upload').click(function (e) {
                e.preventDefault();
                $('#audioFile').click();
            });
        });
    },
    displayListing: function (articles) {
        var active = 'Converted & Active',
                inprogress = 'Conversion In Progress',
                pending = 'Not Converted',
                counter = 0;
        //limits displaying of articles
        $.each(articles, function (k, v) {

            if (typeof v != "undefined") {
                var template = '';
                if ($(window).width() < '768') {
                    template = $('#article-listing-tpl').html().replace(/{alt}/g, counter % 2 ? 'rposition' : '');
                } else {
                    template = $('#article-listing-tpl').html().replace(/{alt}/g, counter % 2 ? 'alt rposition' : '');
                }
                var articleDesc = '';
                var color = '';

                if (v.status == 'active') {
                    articleDesc = active,
                            color = 'green';
                } else if (v.status == 'inprogress') {
                    articleDesc = inprogress,
                            color = 'blue';
                } else {
                    articleDesc = pending,
                            color = 'red';
                }

                var articleTags = '';
                var tags = ((typeof tagsSelected[v.id] != 'undefined') ? tagsSelected[v.id].slice(0, -1) : '');
                if (tags != '') {
                    arrTags = tags.split(',');
                    $.each(arrTags, function (j, k) {
                        articleTags += '<span class="label label-info">' + k + '</span>';
                    });
                } else {
                    articleTags = '';
                }

                //replace all article data in template
                templateContent = $(template).html().replace(/{uuid}/g, v.uuid)
                        .replace(/{article-time}/g, $.timeago(v.createdAt))
                        .replace(/{article-title}/g, v.title)
                        .replace(/{article-url}/g, v.url)
                        .replace(/{article-desc}/g, articleDesc)
                        .replace(/{article-tags}/g, articleTags)
                        .replace(/{hide}/g, v.status != 'active' ? 'hide' : '')
                        .replace(/{archide}/g, v.status != 'active' && v.status != 'pending' ? 'hide' : '')
                        .replace(/{color}/g, color);

                $('.ui-timeline').append($(template).html(templateContent));
                counter++;
            } else {
                $('.acticle-actions').hide();
            }
        });

        this.currentArticle += counter;
        this.triggerOnHover();
    },
    triggerOnHover: function () {
        $('.ti-content-detail').hover(function () {
            $('.ti-content-detail-info', this).animate({top: '-100px'}, {duration: 'slow', easing: 'easeOutBack'});
            $('.ti-content-detail-actions', this).animate({top: '10px'}, {duration: 'slow', easing: 'easeOutBack'});
        },
                function () {
                    $('.ti-content-detail-info', this).animate({top: '15px'}, {duration: 'slow', easing: 'easeOutBack'});
                    $('.ti-content-detail-actions', this).animate({top: '200px'}, {duration: 'slow', easing: 'easeOutBack'});
                });
    },
    displayArchiveList: function (onload, articles) {

        if (typeof articles != "undefined") {
            $('.archived-actions').show();
            var counter = 0;
            $.each(articles, function (k, v) {
                if (typeof v != "undefined") {
                    var template = $('#archive-listing-tpl').html().replace(/{alt}/g, counter % 2 ? 'alt' : '');
                    var articleDesc = 'Archive Article';
                    //replace all article data in template
                    templateContent = $(template).html().replace(/{uuid}/g, v.uuid)
                            .replace(/{article-time}/g, $.timeago(v.createdAt))
                            .replace(/{article-title}/g, v.title)
                            .replace(/{article-desc}/g, articleDesc);

                    $('.ui-timeline').append($(template).html(templateContent));
                    counter++;
                } else {
                    $('.archived-actions').hide();
                }
            });
            Articles.currentArticle += counter;

            this.triggerOnHover();
        } else {
            if (onload) {
                $('.ui-timeline').html('No archive articles found.');
            }
        }
    },
    archived: function (_this, inactive) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/article/archived/' + $(_this).attr('data-uuid'),
            success: function (data) {
                if (data.success) {
                    if (inactive) {
                        alert('Content has been successfully inactive.');
                    } else {
                        alert('Content has been successfully archived.');
                    }
                    All.progress(50, $('#progressBar'));
                    location.href = "/publisher/article";
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            error: function () {
                alert("An error occured. Please refresh the page and try again.");
            }
        });
    },
    reconvert: function (_this) {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: '/publisher/article/reconvert/' + $(_this).attr('data-uuid'),
            success: function (data) {
                if (data.success) {
                    alert('Archive Content has been successfully converted.');
                    location.reload();
                } else {
                    alert("An error occured. Please refresh the page and try again.");
                }
            },
            error: function () {
                alert("An error occured. Please refresh the page and try again.");
            }
        });
    },
    addAds: function (_this) {

        var dataVal = {'article-uuid': $(_this).attr('data-article-uuid'),
            'ads-uuid': $(_this).attr('data-ads-uuid'),
            'action-type': $(_this).attr('data-action-type'),
            'isChecked': $(_this).prop('checked')};

        Articles.adsRequest = $.ajax({
            url: '/publisher/article/saveArticleAds',
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                if (typeof Articles.adsRequest != 'undefined' && Articles.adsRequest != null) {
                    Articles.adsRequest.abort();
                }
            },
            success: function (data) {
                if (data.success === true) {
                    All.progress(100, $('#progressBar'));
                }
            },
            data: dataVal
        });
    },
    addTagging: function (_this) {

        var tagsSelected = Articles.tagsSelected;
        Articles.processTag = $.ajax({
            url: '/publisher/article/saveArticleTags',
            dataType: 'json',
            type: 'POST',
            beforeSend: function () {
                if (typeof Articles.processTag != 'undefined' && Articles.processTag != null) {
                    Articles.processTag.abort();
                }
            },
            success: function (data) {
                if (data.success === true) {
                    All.progress(100, $('#progressBar'));
                }
            },
            data: {data: tagsSelected,
                uuid: $(_this).attr('data-uuid')}
        });
    },
    uploadAudioFile: function() {
        All.progress(50, $('#progressBar'));
        var audioFile = $('.audio-file #files').html();
        var audioDesc = $('#uploadAudioDesc').val();
        var audioUuid = $('#uploadAudioUuid').val();
        var formData = new FormData();
        formData.append('audioFile', audioFile);
        formData.append('audioDesc', audioDesc);
        formData.append('uuid', audioUuid);
        $.ajax({
            url: '/publisher/article/xml/audio',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            type: 'POST',
            success: function (data) {
                $('#uploadAudioFileMessage').hide();
                $('#uploadAudioFileMessage').html("");
                if (data.upload === true) {
                    All.progress(100, $('#progressBar'));
                    $('#uploadAudioFileMessage').append("<li>Upload Audio File Success!</li>");
                    $('#uploadAudioFileMessage').show()
                    Articles.activateSuccess = true;
                } else {
                    if (Object.keys(data.message).length != 0) {
                        for (i in data.message) {
                            $('#uploadAudioFileMessage').append("<li>" + data.message[i] + "</li>");
                        }
                    } else {
                        if (audioFile.length != 0) {
                            $('#uploadAudioFileMessage').html("<li>An error occured. Please reload and try again.</li>");
                        } else {
                            $('#uploadAudioFileMessage').html("<li>Please upload file.</li>");
                        }
                    }
                    $('#uploadAudioFileMessage').show();
                }
            }
        });
    }
}