/**
 * Main Page for Consumer
 * @type type
 */
var MainPage = Backbone.View.extend({
    events :{
        'click #add-playlist'         : 'addPlayList',
        'click #close-modal-playlist' : 'closeModalPlayList'
    },
    initialize: function(data) {
        this.playList       = data.playList;
        this.feedList       = data.feedList;
        this.publicFeedList = data.publicFeedList;
        this.listenTo(this.playList, 'change', this.reloadPage);
        this.listenTo(this.feedList, 'change', this.changeUserStats);
        this.listenTo(this.publicFeedList, 'change', this.changeUserStats);
    },
    addPlayList: function() {        
        this.playList.fetch({
            data: $('#consumer-modal-addplaylist #frmAddPlaylist').serialize(),
            type: 'POST',
        });   
    },
    changeUserStats: function() {
        var feedList       = this.feedList.toJSON()
            publicFeedList = this.publicFeedList.toJSON();
        $('#this-playlist-count .count').html(Object.keys(feedList).length);
        $('#this-profile-following .count').html(Object.keys(publicFeedList).length);
    },
    reloadPage: function() {
        location.reload();
    },
    closeModalPlayList: function() {
        this.$el.find('#txt-playlist').val('');
        this.$el.find('#txt-playlist-desc').val('');
        this.$el.find('#addPlayList').modal('hide');
    }
});