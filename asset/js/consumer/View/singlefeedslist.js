var SingleFeedList = FeedList.extend({
    shuffleSettings: 0,
    feedsCount     : 0,
    audioFeeds     : [],
    events : {
        'blur #feeds-title'                   : 'updateFeedsTitle',
        'click #delete-playlist'              : 'deleteFeeds',
        'click #public-switch-toggle'         : 'makePublic',
        'click #close-modal'                  : 'closeDeleteConfirm',
        'click #switch-toggle'                : 'shuffleOn',
        'click input[name="chkCatalog"]'      : 'addKeyword',
        'click .dropdown dd ul li a'          : 'hideKeywordSelect',
        'click .dropdown dt a'                : 'showKeywordSelect',
        'click .icon-playlist-add'            : 'addFollowList',
        'click .icon-playlist-minus'          : 'deleteFollowList',
        "click .list-item-action"             : 'archivedFeedArticle',
        'click #list-item-name .list-content' : 'playAudio',
        'click #single-list-play'             : 'playAll',
    },
    initialize: function(data){
        
        this.feedsData          = data.feedsDataModel;
        this.feedList           = data.feedList;
        this.category           = data.categoriesModel;
        this.keywordModel       = data.keywordModel;
        this.followModel        = data.followModel;
        this.listTemplate       = data.listTemplate;
        this.reload             = data.reload;
        this.feedsArticleModel  = data.feedsArticleModel;
        this.categoryTemplate   = data.categoryTemplate;
        this.feedsData.set(data.feedData);
        this.feedList.fetch();
        this.initializeSingleSetting();
        this.listenTo(this.feedList, 'change', this.render);
        this.listenTo(this.feedsData, 'change', this.renderFeedsChange);
        this.listenTo(this.feedList, 'change', this.render);
        this.listenTo(this.feedsData, 'change', this.renderFeedsChange);
        this.listenTo(this.feedsArticleModel, 'change', this.reloadPage);
        this.listenTo(this.followModel, 'change', this.reloadPage);
        this.listenTo(this.category, 'change', this.displayCategory);
        this.listenTo(this.audioFeedsModel, 'change', this.renderSingleFeedsList);
    },
    render: function() {
        var feedList     = this.feedList.toJSON(),
            counter      = 1,
            selfObj      = this;
        this.feedsCount  =  Object.keys(feedList).length;
        
        this.category.fetch({
            data: {
                uuid   : this.feedsData.toJSON().uuid,
            },
            type: 'POST',
        });
        
        var selfObj      = this.$el.find('.single-list-list-items'),
            listTemplate = _.template(this.listTemplate)
            self         = this;
        selfObj.html('');
        $.each(feedList, function(key, data){
            data.audioNumber = key;
            data.duration    = data.duration;
            selfObj.append(listTemplate(data));
        });
        
        if ( $('#swiper-content').length != 0 ) {
            swiper = new Swiper('#swiper-content', {resistance: '100%'});
            swiper.on('slideChangeStart', function () {
                $('.nav-items .nav-item').removeClass('active');
                $('.nav-items .nav-item:nth-child('+(swiper.activeIndex+1)+')').addClass('active');
            });
        }
    },
    reloadPage: function(el){
        location.reload();
    },
    updateFeedsTitle: function(el) {
        this.feedsData.fetch({
            data: {
                title : this.$el.find(el.currentTarget).html(),
                uuid  : this.feedsData.toJSON().uuid,
                action: 'update'
            },
            type: 'POST',
        });
    },
    deleteFeeds: function(el) {
        this.feedsData.fetch({
            data: {
                uuid   : this.feedsData.toJSON().uuid,
                action : 'delete' 
            },
            type: 'POST',
        });
    },
    makePublic: function(el) {
        var isChecked = this.$el.find(el.currentTarget).is(':checked');
        this.$el.find('.add-keywords').hide();
        this.$el.find('#add-keyword-list').removeClass('in');
        if (isChecked) {
            this.$el.find('.add-keywords').show();
        }
        
        this.feedsData.fetch({
            data: {
                uuid   : this.feedsData.toJSON().uuid,
                type   : isChecked == true ? 1 : 0,
                action : 'update' 
            },
            type: 'POST',
        });
    },
    closeDeleteConfirm: function() {
        this.$el.find('#confirmDeletePlaylist').modal('hide');
    },
    shuffleOn: function(el) {
        this.shuffleSettings = this.$el.find(el.currentTarget).is(':checked');
    },
    addKeyword: function(el) {
        var title = this.$el.find(el.currentTarget).val() + ",";
        if (this.$el.find(el.currentTarget).is(':checked')) {
            var html = '<span title="' + title + '">' + title + '</span>';
            this.$el.find('.multiSel').append(html);
            this.$el.find(".hida").hide();
        } else {
            this.$el.find('span[title="' + title + '"]').remove();
            if (this.$el.find('input[name="chkCatalog"]').is(':checked') == false) {
                this.$el.find(".hida").show();
            }
        }
        
        var feedUuid     = this.feedsData.toJSON().uuid,
            categoryUuid = this.$el.find(el.currentTarget).attr('data-uuid'),
            isChecked    = this.$el.find(el.currentTarget).is(':checked');
    
        this.keywordModel.fetch({
            data: {
                feedUuid     : feedUuid, 
                categoryUuid : categoryUuid,
                isChecked    : isChecked
            },
            type: 'POST',
        });
    },
    pullRefreshList: function() {
        this.feedList.fetch();
    },
    hideKeywordSelect: function(el) {
        this.$el.find(el.currentTarget).closest(".dropdown dd ul").hide();
    },
    showKeywordSelect: function(el) {
        this.$el.find(".dropdown dd ul").slideToggle('fast');
    },
    addFollowList: function() {
        this.followModel.fetch({
            data: {
                actionType : 'add',
                uuid : SingleFeeds.feedsData.toJSON().uuid
            },
            type: 'POST',
        });
    },
    deleteFollowList: function() {
        this.followModel.fetch({
            data: {
                actionType : 'delete',
                uuid : SingleFeeds.feedsData.toJSON().uuid
            },
            type: 'POST',
        });
    },
    archivedFeedArticle: function(el) {
        var feedId    = this.$el.find(el.currentTarget).attr('data-article-id');
        var articleId = this.$el.find(el.currentTarget).attr('data-feed-id');
        
        this.feedsArticleModel.fetch({
            data: {
                feed:feedId, 
                article: articleId
            },
            type: 'POST',
        });
    },
    displayCategory: function() {
        var category     = this.category.toJSON()
            userCategory = [];
        
        self = this.$el.find('#keywordSelect > ul'),
        categoryTemplate = _.template(this.categoryTemplate);

        $.each(category.category, function(key, data){
            
            if (typeof data.details != 'undefined') {
                data.details.className = 'maincat';
                data.details.check     = '';
            
                if (typeof category.usercategory[data.details.title] != 'undefined') {
                    data.details.check = 'checked';
                    userCategory.push(data.details.title);
                }

                self.append(categoryTemplate(data.details));
                
                $.each(data.child, function(ckey, cval) {
                    cval.className = 'subcat';
                    cval.check     = '';
                    if (typeof category.usercategory[cval.title] != 'undefined') {
                        cval.check = 'checked';
                        userCategory.push(cval.title);
                    }
                    self.append(categoryTemplate(cval));
                });
            }
        });
        selfS = this;
        $.each(userCategory, function(k,v) {
            v += ',';
            var html = '<span title="' + v + '">' + v + '</span>';
            selfS.$el.find('.multiSel').append(html);
            selfS.$el.find(".hida").hide();
        });
    },
    initializeSingleSetting: function() {
        var isChecked = this.$el.find('#public-switch-toggle').is(':checked');
        this.$el.find('.add-keywords').hide();
        this.$el.find('#add-keyword-list').removeClass('in');
        if (isChecked) {
            this.$el.find('.add-keywords').show();
        }
    },
    renderFeedsChange: function() {
        var feedData = this.feedsData.toJSON();
        
        if (feedData.action == 'update') {
            this.$el.find('.pane-title h3').html(feedData.feed.title);
        } else {
            location.href = '/consumer/feeds';
        }
    },
    renderSingleFeedsList: function() {
        var selfObj      = this.$el.find('.single-list-list-items'),
            listTemplate = _.template(this.listTemplate)
            self         = this;
        selfObj.html('');
        $.each(this.audioFeedsModel.toJSON(), function(key, data){
            selfObj.append(listTemplate(data));
        });
    },
    playAudio: function(el) {
        player.playAudio(el);
    },
    playAll: function() {
        player.playAll();
    }
});