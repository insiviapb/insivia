/**
*  TODO: Fix 500 status on trackAudio when there are 2 *mp3s only(eg: preAds, Article)
*
*/
var SearchPlayer = Player.extend({
    playCounter      : 1,
    audioTime        : 0,
    limitResult      : 3,
    offsetResult     : 1,
    eventsExist      : 0,
    totalCountResult : 0,
    itemCounter      : 0,
    events:{
        'click #searchPlay, #search-small-play'  : 'searchPlayPause',
        'keyup #searchKeyword'      : 'newSearch',
        "change #seek, #small-seek" : "contentChanged",
        'click .search-change-size'        : 'changeSize',
        'click #search-result #list-item-name .list-content' : 'playAudio',
        'click .load-more'        : 'loadMoreResult',
    },
    initialize: function(data){
        this.searchArticle       = data.searchArticleModel;
        this.playingDescTemplate = data.playingDescTemplate;
        this.listTemplate        = data.listTemplate;
        this.keyword             = '';
        this.trackAudio         = data.trackAudio;
        this.audioFeedsModel     = new Backbone.Model({});
        _.bindAll(this, 'contentChanged');
        this.listenTo(this.trackAudio, 'change', this.setTrackAudioDesc);
        this.listenTo(this.searchArticle, 'change', this.renderSearchResult);
        
        $('#frmDiscover').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) { 
              e.preventDefault();
              return false;
            }
        });
    },
    newSearch: function(el) {  
        this.offsetResult = 1;
        searchPage.itemCounter = 0;
        this.searchArtcleByKeyword(el, true);
        
    },
    loadMoreResult: function () {
        this.offsetResult += 1;
        this.searchArtcleByKeyword($('#searchKeyword'), false);
    },
    contentChanged: function(el) {
        searchPage.audioFile[0].currentTime = this.$el.find(el.currentTarget).val();
        $("#seek, #seek-small").attr("max", searchPage.audioFile[0].duration);
    },
    renderSearchResult: function() {
        var searchResult = this.searchArticle.toJSON(),
            self = this.$el.find('#search-result'),
            listTemplate = _.template(this.listTemplate);
        
        searchPage.totalCountResult = searchResult.totalCount;
        delete searchResult.totalCount;
        
        
        $.each(searchResult, function(key, data){
            data.audioNumber = key;
            searchPage.itemCounter += 1;
            data.duration    = data.duration;
            self.append(listTemplate(data));
        });
        
        if(searchPage.totalCountResult > searchPage.itemCounter) {
            $('.load-more').show().removeAttr('disabled');
        } else {
            $('.load-more').attr('disabled','disabled');
            if(searchPage.isClick) {
                $('.load-more').hide();
            }
        }
        
        if(Object.keys(searchResult).length > 0) {
            this.searchMessage = '<b>'+ searchPage.totalCountResult +'</b> result(s) for <b>"' + this.keyword + '"</b>';
        } else {
            this.searchMessage = 'No result for: <b>' + this.keyword + '</b>';
        }
        
        $('.search-message').html('').html(this.searchMessage).show();
        
    },
    searchArtcleByKeyword: function(el, isClick) {
        var el_sr = this.$el.find('#search-result');
        searchPage.isClick = isClick;
        if(isClick) {
            el_sr.html('');
            this.keyword = this.$el.find(el.currentTarget).val().trim();
        } else {
            this.keyword  = el.val().trim();
        }
            
        this.searchArticle.clear();
        
        
        if(this.keyword.length > 0) {
            this.searchArticleRequest = this.searchArticle.fetch({
                data: {
                    searchKeyword : this.keyword,
                    limit         : this.limitResult,
                    offset        : this.offsetResult
                },
                type: 'POST',
                beforeSend: function () {
                    if (typeof searchPage.searchArticleRequest != 'undefined' && searchPage.searchArticleRequest != null) {
                        searchPage.searchArticleRequest.abort();
                    }
                },
            });
        } else {
            $('.search-message').hide();
        }
    },
    addAudioEvents: function() {
        searchPage.audioFile[0].addEventListener("loadstart",function(){
            $('#searchPlay, #search-small-play').html('<i class="fa fa-pause-circle"></i>');
        }, false);
        
        searchPage.audioFile[0].addEventListener('timeupdate', function() {
            var value = (parseInt(searchPage.audioFile[0].currentTime)/parseInt(searchPage.audioFile[0].duration))*100;
            var duration = parseFloat(searchPage.audioFile[0].currentTime).toFixed(2) + "/" + parseFloat(searchPage.audioFile[0].duration).toFixed(2) ;
            var durationDisplay = searchPage.formatSeconds(parseFloat(searchPage.audioFile[0].duration).toFixed(2));
            
            if (isNaN(searchPage.audioFile[0].duration) == false) {
                $('.seek-duration').html(durationDisplay);
            }
            
            searchPage.audioTime = parseFloat(searchPage.audioFile[0].currentTime).toFixed(2);
            
            $("input#seek, input#seek-small").val(value);
            $('#file-duration').html(value);
            if ($('#player-view.small').length == 0) {
                $('#player-view').addClass('large');
            }
            
        }, false);
        
        searchPage.audioFile[0].addEventListener("ended", function(){
            searchPage.trackDuration();
            if (Object.keys(searchPage.audioList).length > 0) {
                audio = searchPage.audioList[0];
                this.currentAudio = audio;
                searchPage.trackAudioPlay();
                searchPage.audioList.splice(0,1);
                searchPage.playAudioPlayer(audio, searchPage.audioFile[0]);
            } else {
                searchPage.stopAudio();
            }
            
            $('.fa-pause-circle').replaceWith('<i class="fa fa-play-circle"></i>');
        }, false);
    },
    addAudioList: function(listNo) {
        searchPage.audioList = [];
        audioCounter   = 0;
        if (this.searchArticle.toJSON()[listNo].preAds != null) {
            var preAds = (this.searchArticle.toJSON()[listNo].preAds).split('|');
            searchPage.audioList.push((preAds[1].substr(0,1) == '/') ? this.searchArticle.toJSON()[listNo].s3Link + preAds[1] : '/files/' + this.searchArticle.toJSON()[listNo].audioName);
            audioCounter++;
        }
        if (this.searchArticle.toJSON()[listNo].artFile != null) {
            artFile = this.searchArticle.toJSON()[listNo].artFile.split('|');
            searchPage.audioList.push((artFile[1].substr(0,1) == '/') ? this.searchArticle.toJSON()[listNo].s3Link + artFile[1] : '/files/' + this.searchArticle.toJSON()[listNo].audioName);
            audioCounter++;
        }
        if (this.searchArticle.toJSON()[listNo].postAds != null) {
            var postAds = (this.searchArticle.toJSON()[listNo].postAds).split('|');
            searchPage.audioList.push((postAds[1].substr(0,1) == '/') ? this.searchArticle.toJSON()[listNo].s3Link + postAds[1] : '/files/' + this.searchArticle.toJSON()[listNo].audioName);
            audioCounter++;
        }
        searchPage.audioListCounter = audioCounter;
    },
    playAudio: function(el) {
        var feedsList   = this.$el.find(el.currentTarget)
            this.listNo = feedsList.attr('data-number'),
            listNo      = this.listNo;
            
        searchPage.updatePlayingDesc(el, true);
        
        searchPage.audioFile   = AudioPlayerElement;
        searchPage.addAudioList(listNo);
        searchPage.clearPrevAudio();
        if (Object.keys(searchPage.audioList).length > 0) {
            audio                   = searchPage.audioList[0];
            
            this.currentAudio       = audio;
            this.playCounter = parseInt(listNo)+1;
            this.listNo      = listNo;
            searchPage.trackAudioPlay();
            searchPage.audioList.splice(0,1);
            this.playAudioPlayer(audio, AudioPlayerElement[0]);
            if (!this.eventsExist) {
                this.addAudioEvents();
            }
            this.eventsExist = true;
        }
    },
    trackAudioPlay: function() {
        var audioDetails = [];
        if (searchPage.audioListCounter == 3 && Object.keys(searchPage.audioList).length == 3 ||
            (searchPage.audioListCounter == 2 && Object.keys(searchPage.audioList).length == 2 && this.searchArticle.toJSON()[this.listNo].postAds == null)) {
            audioDetails = this.searchArticle.toJSON()[this.listNo].preAds.split('|');
        }
        if ((searchPage.audioListCounter == 3 && Object.keys(searchPage.audioList).length == 2) ||
            (searchPage.audioListCounter == 2 && Object.keys(searchPage.audioList).length == 2 && this.searchArticle.toJSON()[this.listNo].postAds !== null) ||
            (searchPage.audioListCounter == 2 && Object.keys(searchPage.audioList).length == 1 && this.searchArticle.toJSON()[this.listNo].postAds == null) ||
            (searchPage.audioListCounter == 1 && Object.keys(searchPage.audioList).length == 1)){
            audioDetails = this.searchArticle.toJSON()[this.listNo].artFile.split('|');
        }
        if (searchPage.audioListCounter == 3 && Object.keys(searchPage.audioList).length == 1) {
            audioDetails = this.searchArticle.toJSON()[this.listNo].postAds.split('|');
        }
        searchPage.trackAudio.fetch({
            data: {
                type : audioDetails[2],
                uuid : audioDetails[0]
            },
            type: 'POST',
        });
    },
    searchStop: function(audioPlayer) {
        audioPlayer.currentTime = 0;
        audioPlayer.src = audio;
        
    },
    playAudioPlayer: function(audio, audioEl) {
        audioEl.currentTime = 0;
        audioEl.src = audio;
        
        searchPage.audioFile[0].load();
        searchPage.audioFile[0].play();
    },
    updatePlayingDesc: function(el, isClick){
        if (isClick) {
            var element = this.$el.find(el.currentTarget).closest('#list-item-name');
        } else {
            var element = el;
        }
        var data    = { trackTitle : element.find('.list-item-title').html(),
                        imagePath  : element.find('.list-item-title').attr('data-imgPath'),
                        shortCode  : element.find('.list-item-title').attr('data-shortcode'),
                        trackDesc  : element.find('.list-item-excerpt span').html(),
                        showArticle: this.showArticle == 'up' ? 'down' : 'up',
                        url        : element.find('.article-url').html(),
                        description: element.find('.article-description').html(),
                        duration   : element.find('.duration').html(),
                        feedData : {title: 'Search Result'}
                    },
        
        playingTemplate = _.template(this.playingDescTemplate);
        $('#player-view').html(playingTemplate(data));
        if ($('#player-view.small').length > 0) {
            $('#player-view').attr('class', 'view-7 large');
        }
    },
    trackDuration: function() {
        var durationTime = Math.ceil(searchPage.audioFile[0].currentTime);
        searchPage.trackAudio.fetch({
            data: {
                duration : durationTime,
                trackId  : searchPage.trackAudioDesc.trackId,
                type     : searchPage.trackAudioDesc.type
            },
            type: 'POST',
        });
    },
    setTrackAudioDesc: function() {
        searchPage.trackAudioDesc = searchPage.trackAudio.toJSON();
    },
    
    stopAudio: function() {
        
        if (typeof searchPage.audioFile[0] == 'object') {
            searchPage.audioFile[0].pause();
            searchPage.trackDuration();
            searchPage.audioFile[0].currentTime = 0;
            this.$el.find("#seek, #seek-small").attr("value", searchPage.audioFile[0].currentTime);
            this.$el.find('#searchPlay, #search-small-play').html('<i class="fa fa-play-circle"></i>');
        }
    },
    clearPrevAudio: function() {
        // if(typeof player !== 'undefined') {
        //     if(Object.keys(player.audioList).length > 0) {
        //         player.stopAudio();
        //         player.audioList.splice(0,Object.keys(player.audioList).length);
        //         
        //         
        //     }
        // }
        
        
        // if (typeof searchPage.audioFile[0] == 'object') {
        //     player.audioList.splice(0,Object.keys(player.audioList).length);
        //     searchPage.audioFile[0].pause();
        //     searchPage.audioFile[0].currentTime = 0;
        // }
        
    },
    playOnAudio: function() {
        if (typeof searchPage.audioFile == 'object') {
            searchPage.audioFile[0].play();
        }
    },
    pauseAudio: function() {
        if (typeof searchPage.audioFile == 'object') {
            searchPage.audioFile[0].pause();
        }
    },
    searchPlayPause: function(el) {
        var isExist = this.$el.find(el.currentTarget).find('.fa-play-circle').length;
        if (isExist) {
            searchPage.playOnAudio();
            this.$el.find('#searchPlay, #search-small-play').html('<i class="fa fa-pause-circle"></i>');
        } else {
            searchPage.pauseAudio(el);
            this.$el.find('#searchPlay, #search-small-play').html('<i class="fa fa-play-circle"></i>');
        }
    },
});