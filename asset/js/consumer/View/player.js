/**
 * This function is for Individual Single Playlist
 * @type type
 */
var Player = FeedList.extend({
    playCounter: 1,
    arrPlaying: [],
    showArticle: 'up',
    audioFeeds: [],
    audioList: [],
    audioTime: 0,
    trackAudioDesc: {},
    listNo: 0,
    eventsExist: 0,
    events: {
        'click #play, #small-play': 'playPause',
        'click .fa-stop-circle': 'stopAudio',
        'click .change-size': 'changeSize',
        "change #seek, #small-seek": "contentChanged",
        'click #backward': 'playPreviousAudio',
        'click #forward': 'playNextAudio',
        'click #fbShareButton': 'shareViaFb',
        'click #twitterShareButton': 'shareViaTwitter',
        'click #linkedinShareButton': 'shareViaLinkedIn'
    },
    initialize: function(data) {
        this.feedsData = data.feedsDataModel;
        this.feedList = data.feedList;
        this.playingDescTemplate = data.playingDescTemplate;
        this.trackAudio = data.trackAudio;
        this.audioFeedsModel = new Backbone.Model({});
        _.bindAll(this, 'contentChanged');
        this.listenTo(this.trackAudio, 'change', this.setTrackAudioDesc);
    },
    contentChanged: function(el) {
        this.audioFile[0].currentTime = this.$el.find(el.currentTarget).val();
        $("#seek, #seek-small").attr("max", this.audioFile[0].duration);
    },
    formatSeconds: function(gTime) {
        time = Math.round(gTime);
        var minutes = Math.floor(time / 60);
        var seconds = time - minutes * 60;
        return String(this.pad(minutes, 2)) + '.' + String(this.pad(seconds, 2));
    },
    pad: function(str, max) {
        str = str.toString();
        return str.length < max ? this.pad("0" + str, max) : str;
    },
    playPreviousAudio: function() {

        this.playCounter -= 2;
        if (this.playCounter <= 0) {
            this.playCounter = Object.keys(this.feedList.toJSON()).length;
        }
        this.stopAudio();
        this.playAll();
    },
    playNextAudio: function() {
        this.stopAudio();
        this.playAll();
    },
    playPause: function(el) {
        var isExist = this.$el.find(el.currentTarget).find('.fa-play-circle').length;
        if (isExist) {
            this.playOnAudio();
            this.$el.find('#play, #small-play').html('<i class="fa fa-pause-circle"></i>');
        } else {
            this.pauseAudio(el);
            this.$el.find('#play, #small-play').html('<i class="fa fa-play-circle"></i>');
        }
    },
    addAudioEvents: function() {
        player.audioFile[0].addEventListener("loadstart", function() {
            $('#play, #small-play').html('<i class="fa fa-pause-circle"></i>');
        }, false);

        player.audioFile[0].addEventListener('timeupdate', function() {
            var value = (parseInt(player.audioFile[0].currentTime) / parseInt(player.audioFile[0].duration)) * 100;
            var duration = parseFloat(player.audioFile[0].currentTime).toFixed(2) + "/" + parseFloat(player.audioFile[0].duration).toFixed(2);
            var durationDisplay = player.formatSeconds(parseFloat(player.audioFile[0].duration).toFixed(2));

            if (isNaN(player.audioFile[0].duration) == false) {
                $('.seek-duration').html(durationDisplay);
            }

            player.audioTime = parseFloat(player.audioFile[0].currentTime).toFixed(2);

            $("input#seek, input#seek-small").val(value);
            $('#file-duration').html(value);
            if ($('#player-view.small').length == 0) {
                $('#player-view').addClass('large');
            }

        }, false);

        player.audioFile[0].addEventListener("ended", function() {
            player.trackDuration();
            if (Object.keys(player.audioList).length > 0) {
                audio = player.audioList[0];
                player.currentAudio = audio;
                player.trackAudioPlay();
                player.audioList.splice(0, 1);
                player.playAudioPlayer(audio, player.audioFile[0]);
            } else {
                player.playAll();
            }

            $('.fa-pause-circle').replaceWith('<i class="fa fa-play-circle"></i>');
        }, false);
    },
    updatePlayingDesc: function(el, isClick) {
        if (isClick) {
            var element = SingleFeeds.$el.find(el.currentTarget).closest('#list-item-name');
        } else {
            var element = el;
        }
        var data = {trackTitle: element.find('.list-item-title').html(),
            imagePath: element.find('.list-item-title').attr('data-imgPath'),
            shortCode: element.find('.list-item-title').attr('data-shortcode'),
            trackDesc: element.find('.list-item-excerpt span').html(),
            showArticle: this.showArticle == 'up' ? 'down' : 'up',
            url: element.find('.article-url').html(),
            description: element.find('.article-description').html(),
            duration: element.find('.duration').html()},
        playingTemplate = _.template(this.playingDescTemplate);
        this.$el.html(playingTemplate(data));
        if ($('#player-view.small').length == 0) {
            $('#player-view').addClass('large');
        }
    },
    playOnAudio: function() {

        if (typeof this.audioFile == 'object') {
            this.audioFile[0].play();
        }
    },
    pauseAudio: function() {
        if (typeof this.audioFile == 'object') {
            this.audioFile[0].pause();
        }
    },
    stopAudio: function() {

        if (typeof this.audioFile != 'undefined' && typeof this.audioFile[0] ==  'object') {
            this.audioFile[0].pause();
            this.trackDuration();
            this.audioFile[0].currentTime = 0;
            this.$el.find("#seek, #seek-small").attr("value", player.audioFile[0].currentTime);
            this.$el.find('#play, #small-play').html('<i class="fa fa-play-circle"></i>');
        }
    },
    addAudioList: function(listNo) {
        this.audioList = [];
        audioCounter = 0;
        if (this.feedList.toJSON()[listNo].preAds != null) {
            var preAds = (this.feedList.toJSON()[listNo].preAds).split('|');
            this.audioList.push((preAds[1].substr(0, 1) == '/') ? this.feedList.toJSON()[listNo].s3Link + preAds[1] : '/files/' + this.feedList.toJSON()[listNo].audioName);
            audioCounter++;
        }
        if (this.feedList.toJSON()[listNo].artFile != null) {
            artFile = this.feedList.toJSON()[listNo].artFile.split('|');
            this.audioList.push((artFile[1].substr(0, 1) == '/') ? this.feedList.toJSON()[listNo].s3Link + artFile[1] : '/files/' + this.feedList.toJSON()[listNo].audioName);
            audioCounter++;
        }
        if (this.feedList.toJSON()[listNo].postAds != null) {
            var postAds = (this.feedList.toJSON()[listNo].postAds).split('|');
            this.audioList.push((postAds[1].substr(0, 1) == '/') ? this.feedList.toJSON()[listNo].s3Link + postAds[1] : '/files/' + this.feedList.toJSON()[listNo].audioName);
            audioCounter++;
        }
        this.audioListCounter = audioCounter;
    },
    playAudio: function(el) {
        
        this.stopAudio();
        var feedsList = SingleFeeds.$el.find(el.currentTarget)
        this.listNo = feedsList.attr('data-number'),
                listNo = this.listNo;
        this.updatePlayingDesc(el, true);
        this.addAudioList(listNo);
        if (Object.keys(this.audioList).length > 0) {
            audio = this.audioList[0];
            player.audioFile = AudioPlayerElement;
            this.currentAudio = audio;
            player.playCounter = parseInt(listNo) + 1;
            player.listNo = listNo;
            player.trackAudioPlay();
            player.audioList.splice(0, 1);
            player.playAudioPlayer(audio, AudioPlayerElement[0]);
            if (!player.eventsExist) {
                player.addAudioEvents();
            }
            player.eventsExist = true;
        }
    },
    playAudioPlayer: function(audio, audioPlayer) {
        audioPlayer.currentTime = 0;
        audioPlayer.src = audio;
        player.audioFile[0].load();
        player.audioFile[0].play();
    },
    trackDuration: function() {
        var durationTime = Math.ceil(player.audioFile[0].currentTime);
        player.trackAudio.fetch({
            data: {
                duration: durationTime,
                trackId: this.trackAudioDesc.trackId,
                type: this.trackAudioDesc.type
            },
            type: 'POST',
        });
    },
    trackAudioPlay: function() {
        var audioDetails = [];
        if (player.audioListCounter == 3 && Object.keys(player.audioList).length == 3 ||
                (player.audioListCounter == 2 && Object.keys(player.audioList).length == 2 && player.feedList.toJSON()[this.listNo].postAds == null)) {
            audioDetails = player.feedList.toJSON()[player.listNo].preAds.split('|');
        }
        if ((player.audioListCounter == 3 && Object.keys(player.audioList).length == 2) ||
                (player.audioListCounter == 2 && Object.keys(player.audioList).length == 2 && player.feedList.toJSON()[this.listNo].postAds !== null) ||
                (player.audioListCounter == 2 && Object.keys(player.audioList).length == 1 && player.feedList.toJSON()[this.listNo].postAds == null) ||
                (player.audioListCounter == 1 && Object.keys(player.audioList).length == 1)) {
            audioDetails = player.feedList.toJSON()[player.listNo].artFile.split('|');
        }
        if (player.audioListCounter == 3 && Object.keys(player.audioList).length == 1) {
            audioDetails = player.feedList.toJSON()[player.listNo].postAds.split('|');
        }
        player.trackAudio.fetch({
            data: {
                type: audioDetails[2],
                uuid: audioDetails[0]
            },
            type: 'POST',
        });
    },
    setTrackAudioDesc: function() {
        this.trackAudioDesc = this.trackAudio.toJSON();
    },
    playAll: function() {
        //reset playCounter
        if (player.playCounter > player.feedsCount) {
            player.playCounter = 1;
            player.listNo = 0;
        }
        if (SingleFeeds.shuffleSettings) {
            if (player.arrPlaying.length == 0) {
                for (counter = 0; counter < player.feedsCount; counter++) {
                    player.arrPlaying[counter] = counter + 1;
                }
            }
            var selectedFeed = Math.floor((Math.random() * player.arrPlaying.length) + 1);
            var selectedPlay = selectedFeed - 1;
            player.playCounter = player.arrPlaying[selectedPlay];
            player.arrPlaying.splice(selectedPlay, 1);
        }

        if ($('.single-list-list-items #list-item-name:nth-child(' + player.playCounter + ')').length != 0) {
            var currentPlayer = $('.single-list-list-items #list-item-name:nth-child(' + player.playCounter + ')');
            player.addAudioList(player.playCounter - 1);
            player.updatePlayingDesc(currentPlayer, false);

            if (typeof player.audioFile == 'undefined') {
                player.audioFile = AudioPlayerElement;
                player.addAudioEvents();
            }
            audio = player.audioList[0];
            player.trackAudioPlay();
            player.audioList.splice(0, 1);
            player.playAudioPlayer(audio, $('#audioPlayer')[0]);
            player.playCounter++;
            player.listNo = player.playCounter - 2;
        } else {
            player.playCounter = 1;
        }
    },
    changeSize: function(el) {
        this.showArticle = this.$el.find(el.currentTarget).attr('data-value');
        if (this.showArticle == 'down') {
            this.$el.find(el.currentTarget).find('.fa-angle-down').replaceWith('<i class="fa fa-angle-up"></i>');
            this.$el.attr('class', 'view-7 small');
            this.$el.find(el.currentTarget).attr('data-value', 'up');
            this.$el.find('.currently-playing').show();
            this.$el.find('.playing-from').hide();
            this.$el.find('#fbShareButton').hide();
        } else {
            this.$el.find(el.currentTarget).find('.fa-angle-up').replaceWith('<i class="fa fa-angle-down"></i>');
            this.$el.attr('class', 'view-7 large');
            this.$el.find(el.currentTarget).attr('data-value', 'down');
            this.$el.find('.currently-playing').hide();
            this.$el.find('.playing-from').show();
            this.$el.find('#fbShareButton').show();
        }
    },
    shareViaFb: function(el) {
        var element = this.$el.find(el.currentTarget);
        var _title = element.data('title');
        var _url = httpHost + element.data('url');
        var _img = element.data('imgpath');
        FB.ui({
            method: 'share',
            display: 'popup',
            title: _title,
            picture: _img,
            href: _url,
        }, function(response) {
        });
    },
    shareViaTwitter: function(el) {
        var element = this.$el.find(el.currentTarget);
        var _title = element.data('title');
        var _url = httpHost + element.data('url');
        var twitter_share = share_twitter + _url + "&text=" + _title + " via @" + $('title').html();
        window.open(twitter_share, _title).focus()
    },
    shareViaLinkedIn: function(el) {
        var element = this.$el.find(el.currentTarget);
        var _title = element.data('title');
        var _url = httpHost + element.data('url');
        var linkedin_share = share_linkedin + encodeURI(_url) + "&mini=true&title=" + encodeURI(_title);
        window.open(linkedin_share, _title).focus()
    }
});