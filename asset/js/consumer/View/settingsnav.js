var SettingsNav  = Backbone.View.extend({
    currentField : '',
    events : {
        'click .settings-nav:nth-child(2)' : 'editProfile',
        'click .settings-nav:last'         : 'logout',
        'blur .profile-editor'             : 'updateProfile',
    },
    initialize: function(data) {
        this.profileDetailsModel = data.profileDetails;
        this.listenTo(this.profileDetailsModel, 'change', this.updateDetails);
        this.initUpload();
    },
    logout: function() {
        location.href = '/signout';
    },
    updateProfile: function(self) {
        var dataField = this.$el.find(self.currentTarget).attr('data-field')
            dataValue = this.$el.find(self.currentTarget).html();
        
        if (dataField == 'email' && !this.validateEmail(dataValue)) {
            this.$el.find(self.currentTarget).attr('style','border:1px solid red;');
            return;
        } else {
            if ($.trim(dataValue).length == 0) {
                this.$el.find(self.currentTarget).attr('style','border:1px solid red;');
                return;
            }
            this.$el.find(self.currentTarget).removeAttr('style');
        }
        this.updateProfileProcess = this.profileDetailsModel.fetch({
            beforeSend : function() {
                if (settingsNav.currentField == dataField) {
                    if(typeof settingsNav.updateProfileProcess != 'undefined' && settingsNav.updateProfileProcess != null) {
                        settingsNav.updateProfileProcess.abort();
                    }
                }
            },
           type:'POST',
           data: {field     : dataField,
                  dataValue : dataValue}
        });
    },
    updateDetails: function() {
        var profileDetails = this.profileDetailsModel.toJSON();
        if (profileDetails.success) {
            if (profileDetails.data.field == 'firstName' || profileDetails.data.field == 'lastName') {
                $('.profile-title').html($('span[data-field="firstName"]').html() + ' ' +$('span[data-field="lastName"]').html());
            }
        }
    },
    initUpload: function() {
        $('#userImage').fileupload({
            url: '/consumer/feeds/process/updateUserPhoto',
            dataType: 'json',
            done: function (e, data) {
                $('#userImage').removeAttr('style');
                if (data.result.success) {
                    $('.profile-image > img').attr('src', data.result.userImage);
                } else {
                    $('#userImage').attr('style','border:1px solid red;');
                }
            },
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    },
    validateEmail : function(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( email );
    }
});