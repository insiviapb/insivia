var BrowseFeedList = FeedList.extend({
    initialize: function(data){
        this.feedList     = data.feedList;
        this.listTemplate = data.listTemplate;
        this.reload       = data.reload;
        this.feedList.fetch();
        this.listenTo(this.feedList, 'change', this.render);
    },
});