/**
 * This function is for the Main PlayList
 * @type type
 */
var FeedList = Backbone.View.extend({
    events :{
        'click #list-item-name' : 'selectFeeds',
    },
    initialize: function(data){
        
        this.feedList     = data.feedList;
        this.listTemplate = data.listTemplate;
        this.reload       = data.reload;
        this.feedList.fetch();
        this.listenTo(this.feedList, 'change', this.render);
    },
    render: function() {
        
        var feedList = this.feedList.toJSON(),
            self = this.$el,
            listTemplate = _.template(this.listTemplate);
        $.each(feedList, function(key, data){
            self.prepend(listTemplate(data));
        });
        
        breakpoint = {
            // when window width is <= 320px
            320: {
              slidesPerView: 1,
              spaceBetweenSlides: 10
            },
            // when window width is <= 480px
            480: {
              slidesPerView: 2,
              spaceBetweenSlides: 10
            },
            // when window width is <= 640px
            640: {
              slidesPerView: 3,
              spaceBetweenSlides: 10
            },
            800: {
              slidesPerView: 4,
              spaceBetweenSlides: 10
            },
            1000: {
              slidesPerView: 5,
              spaceBetweenSlides: 10
            }
        };

        if ($('#user-list').length != 0) {
            userlist = new Swiper('#user-list', {
                slidesPerView:8, 
                spaceBetween: 10,
                breakpoints: breakpoint,
                mode: 'horizontal'
            });
            $('#user-list').scroll(function(){
                $('#user-list .swiper-slide').removeClass('swiper-no-swiping')
            }).mouseenter(function(){
                $('#user-list .swiper-slide').removeClass('swiper-no-swiping')
            }).mouseleave(function(){
                $('#user-list .swiper-slide').removeClass('swiper-no-swiping')
            });
        }
        
        if ($('#public-list').length != 0) {
            
            publiclist = new Swiper('#public-list', {
                slidesPerView:8, 
                spaceBetween: 10,
                breakpoints: breakpoint,
                mode: 'horizontal'
            });
            $('#user-list').scroll(function(){
                $('#public-list .swiper-slide').removeClass('swiper-no-swiping')
            }).mouseenter(function(){
                $('#public-list .swiper-slide').removeClass('swiper-no-swiping')
            }).mouseleave(function(){
                $('#public-list .swiper-slide').removeClass('swiper-no-swiping')
            });
        }
    },
    selectFeeds: function(el) {
        if (this.reload) {
            var url = this.$el.find(el.currentTarget).find('.list-content').attr('data-url');
            location.href = url;
        }
    },
});