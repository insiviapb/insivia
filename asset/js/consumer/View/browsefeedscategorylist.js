var BrowseFeedCategoryList = FeedList.extend({
        initialize: function(data){
            this.feedCategoryList = data.feedListCategory;
            this.listTemplate     = data.listTemplate;
            this.listTemplate2    = data.listTemplate2;
            this.reload           = data.reload;
            this.browseTemplate   = data.browseTemplate;
            this.feedCategoryList.fetch();
            this.listenTo(this.feedCategoryList, 'change', this.renderTopCategory);
        },
        renderTopCategory: function() {
            var feedList       = this.feedCategoryList.toJSON(),
                listTemplate2  = _.template(this.listTemplate2),
                listTemplate   = _.template(this.listTemplate),
                browseTemplate = _.template(this.browseTemplate)
                _this          = this;
            $.each(feedList, function(key, data){
                categoryHtml     = '';
                categoryHtmlData = '';
                $.each(data.feeds, function(key2, data2) {
                    categoryHtmlData += listTemplate(data2);
                });
                categoryHtml += categoryHtmlData;
                data.category_feeds_data = categoryHtml;
                $('#swiper-content .swiper-wrapper .discovery-pane').before(browseTemplate({ categoryTemplate : listTemplate2(data)}));
            });
            
            this.renderSwiper();
        },
        renderSwiper: function() {
            if ( $('#swiper-content').length != 0 ) {
                swiper = new Swiper('#swiper-content', {
                    resistance: '100%'
                });
                swiper.on('slideChangeStart', function () {
                    $('.nav-items .nav-item').removeClass('active');
                    dataNav = $('.main-nav.swiper-slide-active').attr('data-nav');                    
                    $('.nav-items .nav-item:nth-child('+(dataNav)+')').addClass('active');
                });
            }
        }
    });