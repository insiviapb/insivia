var MainNav      = Backbone.View.extend({
    events: {
        'click #lists-btn' : 'reloadMainList',
        'click .nav-item'  : 'reloadNav',
        'click #browse-btn': 'reloadTopList',
    },
    initialize:function() {
        this.render();
    },
    render: function() {
        $('.view').show();
        $('#main-nav').show();
        $('#main-view').show();
    },
    reloadNav: function(el) {
        var element = this.$el.find(el.currentTarget)
            tgt     = element.attr('data-link');
        this.$el.find('.nav-item').removeClass('active');
        element.toggleClass('active');
        if (tgt != 'main-view') {
            $('#'+tgt).show();
        }
        swiper.slideTo($('#'+tgt).closest('.swiper-slide').index(), 500);
    },
    reloadMainList: function() {
        if (window.location.pathname != '/consumer/feeds') {
            location.href = '/consumer/feeds';
        } else {
            this.render();
        }
    }
});