var SearchArticle = Backbone.Model.extend({
    url : '/consumer/feeds/process/getSearchFeedList',
    success: function(c) {
        this.set(c);
    }
});

var trackAudioSearchModel = Backbone.Model.extend({
    url : '/consumer/feeds/process/trackAudio'
});