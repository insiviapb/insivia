if ($('#single-list-view').length > 0) {
    var singleFeedList = Backbone.Model.extend({
        url : '/consumer/feeds/process/getSingleFeedList/' + singleFeedUuid,
    });
    var feedsDataModel = Backbone.Model.extend({
        url : '/consumer/feeds/process/singleSetting/update'
    });
    var feedsArticleModel = Backbone.Model.extend({
        url : '/consumer/feeds/process/archiveFeedsArticle'
    });
    var categoriesModel = Backbone.Model.extend({
        url : '/consumer/feeds/process/getCategory'
    });
    var keywordModel = Backbone.Model.extend({
        url : '/consumer/feeds/process/addKeywords'
    });
    var trackAudioModel = Backbone.Model.extend({
        url : '/consumer/feeds/process/trackAudio'
    });
    var followModel = Backbone.Model.extend({
        url : '/consumer/feeds/process/followPlaylist'
    });
}