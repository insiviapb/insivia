var share_twitter      = config.share_twitter;
var share_linkedin     = config.share_linkedin;
var AudioPlayerElement = $('#audioPlayer');
var settingsNav  = new SettingsNav({
    el             : $('#list-setting-view'),
    profileDetails : profileDetails
});
var mainNav      = new MainNav({
    el : $('#main-nav')
});

if ($('#main-feeds-page').length > 0) {
    var mainPage = new MainPage({
        el : $('#modal-view'),
        playList       : new addPlayList,
        feedList       : usrFeedList,
        publicFeedList : publicFeedList
    });
}

var searchModel = new SearchArticle()
// Search Tab
if ($('#discovery-view').length > 0) {
    var searchPage = new SearchPlayer({
        el              : $('#discovery-view'),
        searchArticleModel: searchModel,
        trackAudio  : new trackAudioSearchModel(),
        playingDescTemplate : $('#search-playing-description-template').html(),
        listTemplate    : $('#single-list-template').html(),
    });

    var searchAudioPlay = new SearchPlayer({
       el          : $('#player-view'),
    });

}


if ($('#user-pane').length > 0) {
    var Feeds = new FeedList({
       el           : $('#user-list .swiper-wrapper'),
       feedList     : usrFeedList,
       listTemplate : $('#user-list-template').html(),
       reload       : true
    });
}

if ($('#public-pane').length > 0) {
    var PublicFeeds = new FeedList({
       el           : $('#public-list .swiper-wrapper'),
       feedList     : publicFeedList,
       listTemplate : $('#follow-list-template').html(),
       reload       : true
    });
}

if ($('#browse-pane').length > 0) {
        
    var BrowseFeeds = new BrowseFeedList({
       el               : $('#browse-list'),
       feedList         : new topFeedList(),
       listTemplate     : $('#follow-list-template').html(),
       reload           : true
    });
    
    var BrowseFeedCategory = new BrowseFeedCategoryList({
       el               : $('#category-list'),
       feedListCategory : new topFeedListByCategory(),
       listTemplate     : $('#follow-list-template').html(),
       listTemplate2    : $('#feedcategory-list-template').html(),
       reload           : true,
       browseTemplate   : $('#browse-cat-slide').html()
    });
}

if ($('#single-list-view').length > 0) {
    
    var modelfeedList = new singleFeedList();
    
    var player = new Player({
       el          : $('#player-view'),
       feedList    : modelfeedList,
       trackAudio  : new trackAudioModel(),
       playingDescTemplate : $('#playing-description-template').html(),
    });
    
    var SingleFeeds = new SingleFeedList({
        el                  : $('#single-list-view'),
        feedList            : modelfeedList,
        listTemplate        : $('#single-list-template').html(),
        categoryTemplate    : $('#category-li-template').html(),
        reload              : false,
        feedsDataModel      : new feedsDataModel(),
        feedsArticleModel   : new feedsArticleModel(),
        categoriesModel     : new categoriesModel(),
        feedData            : feedData,
        keywordModel        : new keywordModel(),
        followModel         : new followModel()
    });    
}

$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
});


$(function(){
    $('#xpullList').xpull({
        'callback':function(){
            SingleFeeds.pullRefreshList();
            // console.log('Released...');
        }
    });		
    
    if($('#activateBanner').length > 0) {
        $('#activateBanner').parent('header').css('height','initial');
    }
    $('.add-new-playlist').click(function(){
        $('#consumer-modal-addplaylist').html($('.add-playlist-body').html());
    });
});
