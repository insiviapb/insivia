$('#frmConsumerRegister').validate({
    onkeyup: function(element) { $(element).valid(); },
    onfocusout: function(element) { $(element).valid(); },
    rules: {
        firstName : "required",
        lastName  : "required",
        email: {
            required : true,
            email    : true,
            notEqualTo : '#username'
        },
        password: {
            required  : true,
            minlength : 5
        },
        passwordVerify: {
            required  : true,
            minlength : 5,
            equalTo: "#password"
        }
    },
    messages: {
        firstName: {
            required: "Please enter your firstname",
        },
        lastName: {
            required: "Please enter your lastname",
        },
        password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
        },
        passwordVerify: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
        },
        email: {
            required: "Please enter email address",
            email: "Please enter a valid email address",
            notEqualTo : 'Please enter email not the same value as username.'
        },
    },
    errorElement: 'label',
    errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
            
    },
    submitHandler: function(form) {
        $('input[name="username"]').val($('#email').val().replace(/@.*/g, ''));
        $('#frmConsumerRegister').submit();
    }
});

$('#frmVoiceTalentRegister').validate({
    onkeyup: function(element) { $(element).valid(); },
    onfocusout: function(element) { $(element).valid(); },
    rules: {
        firstName : "required",
        lastName  : "required",
        email: {
            required : true,
            email    : true,
            notEqualTo : '#username'
        },
        password: {
            required  : true,
            minlength : 5
        },
        passwordVerify: {
            required  : true,
            minlength : 5,
            equalTo: "#password"
        }
    },
    messages: {
        firstName: {
            required: "Please enter your firstname",
        },
        lastName: {
            required: "Please enter your lastname",
        },
        password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long"
        },
        passwordVerify: {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long",
            equalTo: "Please enter the same password as above"
        },
        email: {
            required: "Please enter email address",
            email: "Please enter a valid email address",
            notEqualTo : 'Please enter email not the same value as username.'
        },
    },
    errorElement: 'label',
    errorPlacement: function (error, element) {
        error.insertAfter(element.parent());

    },
    submitHandler: function(form) {
        $('input[name="username"]').val($('#email').val().replace(/@.*/g, ''));
        $('#frmVoiceTalentRegister').submit();
    }
});


$('#frmPublisherRegister').validate({
    onkeyup: function(element) { $(element).valid(); },
    onfocusout: function(element) { $(element).valid(); },
    rules: {
        firstName : "required",
        lastName  : "required",
        email: {
            required : true,
            email    : true,
            notEqualTo : '#username'
        },
        password: {
            required  : true,
            minlength : 5
        },
        passwordVerify: {
            required  : true,
            minlength : 5,
            equalTo: "#password"
        },
    },
    messages: {
        firstName: {
            required: "Please enter your firstname",
        },
        lastName: {
            required: "Please enter your lastname",
        },
        password: {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long"
        },
        passwordVerify: {
            required: "Please provide a password",
            minlength: "Your password must be at least 5 characters long",
            equalTo: "Please enter the same password as above"
        },
        email: {
            required: "Please enter email address",
            email: "Please enter a valid email address",
            notEqualTo : 'Please enter email not the same value as username.'
        },
    },
    errorElement: 'label',
    errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
            
    },
    submitHandler: function(form) {
        $('input[name="username"]').val($('#email').val().replace(/@.*/g, ''));
        $('#frmPublisherRegister').submit();
    }
});
$.validator.addMethod("notEqualTo", function(value, element, param) {
    return this.optional(element) || value != $(param).val();
}, "This has to be different...");