<?php
return array(
//    'router' => array(
//        'routes' => array(
//            'signout' => array(
//                'type' => 'Zend\Mvc\Router\Http\Literal',
//                'options' => array(
//                    'route'    => '/signout',
//                    'defaults' => array(
//                        'controller' => 'zfcuser',
//                        'action'     => 'logout',
//                    ),
//                ),
//            ),
//            'forgot' => array(
//                'type' => 'Zend\Mvc\Router\Http\Literal',
//                'options' => array(
//                    'route'    => '/forgot',
//                    'defaults' => array(
//                        'controller' => 'goalioforgotpassword_forgot',
//                        'action'     => 'forgot',
//                    ),
//                ),
//            ),
//            'roleswitcher' => array(
//                'type' => 'Zend\Mvc\Router\Http\Literal',
//                'options' => array(
//                    'route'    => '/roleswitcher',
//                    'defaults' => array(
//                        'controller' => 'web.controller.role',
//                        'action'     => 'switcher',
//                    ),
//                ),
//            ),
//        ),
//    ),
//    'controllers' => array(
//        'invokables' => array(
//            'web.controller.role' => 'Insivia\Controller\RoleController',
//        ),
//    ),
//    'view_manager' => array(
//        'template_path_stack' => array(
//            __DIR__ . '/../view',
//        ),
//        'template_map' => array(
//            'zfc-user/user/login' => __DIR__ . '/../view/zfc-user/user/login.phtml',
//            'layout/web'  => __DIR__ . '/../view/layout/layout.phtml',
//        ),
//        'strategies' => array(
//            'ViewFeedStrategy',
//        ),
//    ),
//    'asset_manager' => array(
//        'resolver_configs' => array(
//            'paths' => array(
//                __DIR__ . '/../asset',
//            ),
//        ),
//    ),
//    'service_manager' => array(
//        'factories' => array(
//            'web.email.transport' => 'Web\Service\Email\Transport\SmtpFactory',
//            'zfcuser_redirect_callback' => 'Web\Service\Factory\RedirectCallbackFactory'
//        ),
//        'invokables' => array(
//            'lisyn.role' => 'Web\Service\Role'
//        ),
//        'aliases' => array(
//        ),
//    ),
//    'view_helpers' => array(
//        'invokables' => array(
//            'roleSwitcher' => 'Web\View\Helper\RoleSwitcher',
//            'gravatar' => 'Web\View\Helper\Gravatar',
//        ),
//    ),
//    'module_layouts' => array(
//        'Publisher'     => 'layout/publisher',
//        'Article'       => 'layout/publisher',
//        'Ads'           => 'layout/publisher',
//        'Web'           => 'layout/web',
//        'Consumer'      => 'layout/web',
//        'VoiceTalent'   => 'layout/voicetalent',
//    ),
//    'role_switcher' => array(
//        'action' => 'roleswitcher'
//    ),
//    'module_acl' => array(
//        'white_list' => array(
//            'feed',
//            'zfcuser/login',
//            'consumer/login',
//            'publisher/login',
//            'consumer/embed'
//        )
//    ),
);
